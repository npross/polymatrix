Package Contents
================

.. automodule:: polymatrix
   :members:
   :undoc-members:
   :show-inheritance:


PolyMatrix Module
=================

.. automodule:: polymatrix.polymatrix
   :members:
   :undoc-members:
   :show-inheritance:

Abstract Base Classes
---------------------

.. automodule:: polymatrix.polymatrix.abc
   :members:
   :undoc-members:
   :show-inheritance:

Mixins
------

.. automodule:: polymatrix.polymatrix.mixins
   :members:
   :undoc-members:
   :show-inheritance:

Index Types
-----------

.. automodule:: polymatrix.polymatrix.index
   :members:
   :undoc-members:
   :show-inheritance:

..
        Mixin implementations
        ---------------------

        .. automodule:: polymatrix.polymatrix.impl
           :members:
           :undoc-members:
           :show-inheritance:

        .. automodule:: polymatrix.polymatrix.init
           :members:
           :undoc-members:
           :show-inheritance:

Expression Module
=================

.. automodule:: polymatrix.expression.expression
   :members:
   :undoc-members:
   :show-inheritance:

Expression State Module
=======================

.. automodule:: polymatrix.expressionstate.mixins
   :members:
   :undoc-members:
   :show-inheritance:
