from __future__ import annotations

from scipy import sparse

from typing import Iterable, TYPE_CHECKING

from polymatrix.polymatrix.impl import (
        BroadcastPolyMatrixImpl,
        BlockPolyMatrixImpl,
        PolyMatrixImpl,
        SlicePolyMatrixImpl,
        PolyMatrixAsAffineExpressionImpl)

from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MatrixIndex, MonomialIndex, VariableIndex
from polymatrix.polymatrix.mixins import BlockPolyMatrixMixin, PolyMatrixAsAffineExpressionMixin

if TYPE_CHECKING:
    from polymatrix.polymatrix.mixins import BroadcastPolyMatrixMixin, PolyMatrixMixin, SlicePolyMatrixMixin


def init_poly_matrix(
    data: PolyMatrixDict,
    shape: tuple[int, int],
):
    # This is here because the old code in expression.mixins usually gives raw dictionaries
    if not isinstance(data, PolyMatrixDict):
        wrapped = PolyMatrixDict.empty()
        for entry, poly in data.items():
            p = PolyDict.empty()
            for monomial, coeff in poly.items():
                idx = MonomialIndex(VariableIndex(*v) for v in monomial)
                p[idx] = coeff
            wrapped[entry] = p
        data = wrapped

    return PolyMatrixImpl(data=data, shape=shape)


def init_broadcast_poly_matrix(
    data: PolyDict,
    shape: tuple[int, int],
) -> BroadcastPolyMatrixMixin:
    # This is here because the old code in expression.mixins usually gives raw dictionaries
    if not isinstance(data, PolyDict):
        p = PolyDict.empty()
        for monomial, coeff in data.items():
            idx = MonomialIndex(VariableIndex(*v) for v in monomial)
            p[idx] = coeff
        data = p

    return BroadcastPolyMatrixImpl(polynomial=data, shape=shape)


def init_block_poly_matrix(blocks: dict[tuple[range, range], PolyMatrixMixin]) -> BlockPolyMatrixMixin:
    """
    Blocks are given as dictionary from row and column ranges to polymatrices.
    """
    # Check that ranges are correct, and compute shape of whole thing
    nrows, ncols = 0, 0

    for (row_range, col_range), block in blocks.items():
        block_nrows, block_ncols = block.shape
        if (row_range.stop - row_range.start) != block_nrows:
            raise ValueError(f"Row range {row_range} given for block "
                             f"with shape {block.shape} is incorrect.")

        if nrows < row_range.stop:
            nrows = row_range.stop

        if (col_range.stop - col_range.start) != block_ncols:
            raise ValueError(f"Column range {col_range} given for block "
                             f"with shape {block.shape} is incorrect.")

        if ncols < col_range.stop:
            ncols = col_range.stop

    return BlockPolyMatrixImpl(blocks, shape=(nrows, ncols))


def init_slice_poly_matrix(
    reference: PolyMatrixMixin,
    slices: int | slice | range | tuple[int | Iterable[int], int | Iterable[int]]
) -> SlicePolyMatrixMixin:

    # For example v[0] it is implicitly converted to v[0, :]
    if isinstance(slices, int | slice | range):
        slices = (slices, slice(None, None, None))

    formatted_slices: list[tuple] = [(), ()]
    shape = [0, 0]

    # FIXME: see comment on HashableSlice class
    from polymatrix.expression.mixins.sliceexprmixin import HashableSlice

    for i, (what, el, numel) in enumerate(zip(("Row", "Column"), slices, reference.shape)):
        if isinstance(el, int):
            if not (0 <= el < numel):
                raise IndexError(f"{what} {el} is out of range in shape {reference.shape}.")

            # convert to tuple, and we are done
            formatted_slices[i] = (el,)
            shape[i] = 1

        elif isinstance(el, slice | HashableSlice):
            # convert to range
            el = range(el.start or 0, el.stop or numel, el.step or 1)

            if not (0 <= el.start < numel):
                raise IndexError(f"{what} start {el} is out of range in shape {reference.shape}.")

            # range does not include stop
            if not (0 <= el.stop <= numel):
                raise IndexError(f"{what} stop {el} is out of range in shape {reference.shape}.")

            formatted_slices[i] = tuple(el)
            shape[i] = len(formatted_slices[i])

        elif isinstance(el, tuple):
            formatted_slices[i] = el
            shape[i] = len(el)

        else:
            raise TypeError("{what} {el} of type {type(el)} is not a valid slice type.")

    return SlicePolyMatrixImpl(reference=reference, shape=tuple(shape), slice=tuple(formatted_slices))


# FIXME: rename to init_affine_expression
def to_affine_expression(p: PolyMatrixMixin) -> PolyMatrixAsAffineExpressionMixin:
    """ Convert a polymatrix into a PolyMatrixAsAffineExpressionMixin. """
    if isinstance(p, PolyMatrixAsAffineExpressionMixin):
        # No need to convert
        return p

    # get all monomials that come up in p, and sort them
    monomials = tuple(sorted(set(m for _, entry in p.entries() for m
                                 in entry.monomials())))

    # get all variables that come up in expression, and sort them
    variables = tuple(sorted(set(v.index
                                 for m in monomials
                                 for v in m)))

    max_degree = max(m.degree for m in monomials) 

    # Prepare big array
    nrows, ncols = p.shape
    data = sparse.lil_array((nrows, ncols * len(monomials)))
    
    # Generate slices to index sub-matrices
    slices = {
        m: range(i*ncols, (i+1)*ncols)
        for i, m in enumerate(monomials)
    }

    # Copy coefficients
    for (row, col), poly in p.entries():
        for monomial, coeff in poly.terms():
            data[row, slices[monomial].start + col] += coeff

    return PolyMatrixAsAffineExpressionImpl(
            affine_coefficients=data.tocsc(),
            shape=p.shape,
            slices=slices,
            degree=max_degree,
            variable_indices=variables)
