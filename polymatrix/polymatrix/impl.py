from dataclassabc import dataclassabc
from itertools import groupby

from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin, BroadcastPolyMatrixMixin, BlockPolyMatrixMixin, SlicePolyMatrixMixin, PolyMatrixAsAffineExpressionMixin
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MonomialIndex


@dataclassabc(frozen=True)
class PolyMatrixImpl(PolyMatrix):
    data: PolyMatrixDict
    shape: tuple[int, int]

    def __str__(self) -> str:
        nrows, ncols = self.shape
        columns = []
        for c in range(ncols):
            rows, row_width = [], 0
            for r in range(nrows):
                if (r, c) in self.data:
                    entry = str(self.data[r, c])
                    rows.append(entry)

                    if len(entry) > row_width:
                        row_width = len(entry)
                else:
                    rows.append("0")
            columns.append(tuple(row.rjust(row_width) for row in rows))

        row_strings = []
        for r in range(nrows):
            column = ",  ".join(columns[c][r] for c in range(ncols))
            row_strings.append(f"[ {column} ]")

        return "\n".join(row_strings)


@dataclassabc(frozen=True)
class BroadcastPolyMatrixImpl(BroadcastPolyMatrixMixin):
    polynomial: PolyDict
    shape: tuple[int, int]

    def __str__(self):
        return f"broadcast({self.polynomial}, {self.shape})"


@dataclassabc(frozen=True)
class BlockPolyMatrixImpl(BlockPolyMatrixMixin):
    blocks: dict[tuple[range, range], PolyMatrixMixin]
    shape: tuple[int, int]

    # FIXME: added for debugging, but ugly af
    def __str__(self):
        string = "block(\n"

        def by_row(ranges):
            return ranges[0].start
        
        def by_col(ranges):
            return ranges[1].start

        rows_strings = []
        for r, row in groupby(sorted(self.blocks.keys(), key=by_row), key=by_row):
            cols_strings = []
            for blk in sorted(row, key=by_col):
                cols_strings.append(str(self.blocks[blk]))
            rows_strings.append(",\n".join(cols_strings))

        return string + "\n".join(rows_strings) + ")"


@dataclassabc(frozen=True)
class SlicePolyMatrixImpl(SlicePolyMatrixMixin):
    reference: PolyMatrixMixin
    shape: tuple[int, int]
    slice: tuple[tuple[int, ...], tuple[int, ...]]


@dataclassabc(frozen=True)
class PolyMatrixAsAffineExpressionImpl(PolyMatrixAsAffineExpressionMixin):
    affine_coefficients: PolyMatrixAsAffineExpressionMixin.MatrixType
    shape: tuple[int, int]
    slices: dict[MonomialIndex, PolyMatrixAsAffineExpressionMixin.MatrixType]
    degree: int
    variable_indices: tuple[int]

    # def __str__(self):
    #     # FIXME: proper representation with coefficients
    #     string = ""
    #     for m, s in self.slices.items():
    #         ...
    #         
