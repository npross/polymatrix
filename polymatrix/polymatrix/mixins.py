from __future__ import annotations

import typing
import functools
import math
import numpy as np
import scipy.sparse as sparse

from abc import ABC, abstractmethod
from collections import Counter

from scipy.sparse import csc_array
from numpy.typing import ArrayLike, NDArray
from typing import Iterable, Sequence, Callable
from typing_extensions import override

from polymatrix.polymatrix.index import PolyDict, PolyMatrixDict, MatrixIndex, MonomialIndex, VariableIndex
from polymatrix.utils.deprecation import deprecated

class PolyMatrixMixin(ABC):
    """
    Matrix with polynomial entries.
    """

    @property
    @abstractmethod
    def shape(self) -> tuple[int, int]: ...

    @abstractmethod
    def at(self, row: int, col: int) -> PolyDict:
        """ Return the polynomial at the entry (row, col).
        If the entry is zero it returns an empty `PolyDict`, i.e. an empty
        dictionary `{}`.

        .. code:: py

            p.at(row, col) # if you have row and col
            p.at(*entry) # if you have a MatrixIndex
        """

    def scalar(self) -> PolyDict:
        """
        If the polymatrix is scalar, that is, a 1x1 matrix, this is a shorthand
        for .at(0,0) for readability. Raises an error polymatrix is not scalar.
        """
        if self.shape != (1,1):
            raise AttributeError("Use of .scalar() is not allowed because "
                                 f"it has shape {self.shape}")

        return self.at(0, 0)

    def entries(self) -> Iterable[tuple[MatrixIndex, PolyDict]]:
        """
        Iterate over the non-zero entries of the polynomial matrix.

        Typical usage looks like this (`p` is a polymatrix):

        .. code:: py

            for entry, poly in p.entries():
                for monomial, coeff in poly.terms():
                    # do something with coefficients
        """
        nrows, ncols = self.shape
        for row in range(nrows):
            for col in range(ncols):
                if poly := self.at(row, col):
                    yield MatrixIndex(row, col), poly

    def variables(self) -> set[VariableIndex]:
        """
        Get the indices of all variable indices that are in the polymatrix
        object. Only scalar VariableIndices are returned, i.e. they all have a
        power of one.
        """
        variables = set()
        for _, poly in self.entries():
            for monomial, _ in poly.terms():
                for var in monomial:
                    variables.add(VariableIndex(var.index, power=1))

        return variables

    # -- Old API ---

    @deprecated("Replaced by PolyMatrixMixin.entries()")
    def gen_data(self) -> typing.Generator[tuple[tuple[int, int], PolyDict], None, None]:
        yield from self.entries()
 
    @deprecated("Replaced by PolyMatrixMixin.at(row, col).")
    def get_poly(self, row: int, col: int) -> PolyDict | None:
        # NOTE: in the new API when there is nothing at an entry the at()
        # function returns an empty dictionary. This is because emtpy
        # dicionaries are falsy so you can do
        #
        # `if p.at(row, col):`
        #
        # like it was possible with get_poly. However this is better because
        # id does not make use of a null (None) type.
        return self.at(row, col) or None


class PolyMatrixAsDictMixin(PolyMatrixMixin, ABC):
    """ Matrix with polynomial entries, stored as a dictionary. """

    @property
    @abstractmethod
    def data(self) -> PolyMatrixDict:
        """" Get the dictionary. """

    @override
    def at(self, row: int, col: int) -> PolyDict:
        """ See :py:meth:`PolyMatrixMixin.at`. """
        # Check bounds
        if not (0 <= row < self.shape[0]):
            raise IndexError(f"Row {row} out of range, shape is {self.shape}")

        if not (0 <= col < self.shape[1]):
            raise IndexError(f"Column {col} out of range, shape is {self.shape}")

        return self.data.get(MatrixIndex(row, col)) or PolyDict.empty()

    # -- Old API ---

    @override
    def get_poly(self, row: int, col: int) -> PolyDict | None:
        if (row, col) in self.data:
            return self.data[row, col]
        return None


class BroadcastPolyMatrixMixin(PolyMatrixMixin, ABC):
    """
    TODO: docstring, similar to numpy broadcasting.
    https://numpy.org/doc/stable/user/basics.broadcasting.html
    """
    @property
    @abstractmethod
    def polynomial(self) -> PolyDict:
        """ Scalar polynomial that is broadcasted. """

    @override
    def at(self, row: int, col: int) -> PolyDict:
        """ See :py:meth:`PolyMatrixMixin.at`. """
        return self.polynomial

    @override
    def variables(self) -> set[VariableIndex]:
        # Override to be more efficient
        variables = set()
        for monomial, _ in self.polynomial.terms():
            for var in monomial:
                variables.add(VariableIndex(var.index, power=1))

        return variables

    # --- Old API ---

    @override
    def get_poly(self, col: int, row: int) -> PolyDict | None:
        return self.polynomial or None


class BlockPolyMatrixMixin(PolyMatrixMixin, ABC):
    """
    Polymatrix that is made of other blocks, which themselves are polymatrices.
    In other words concatenation of matrices, a generalization of vstack, block
    diagonal, etc. If a block is not present it is filled with zeros.
    """
    # Something like this
    #
    # [[ block11, block12, ..., block1m ]
    #  [ block21, block22, ..., block2m ]
    #    ...
    #  [ blockn1, blockn2, ..., blocknm ]]

    @property
    @abstractmethod
    def blocks(self) -> dict[tuple[range, range], PolyMatrixMixin]:
        """ The blocks. """
        # tuple of ranges are row and column ranges that are covered by each block
        # This class does not check that the blocks are compatible with each other!
        # That must be done at time of construction, see init_block_poly_matrix

    @override
    def at(self, row: int, col: int) -> PolyDict:
        if not (0 <= row < self.shape[0]):
            raise IndexError(f"Row {row} out of range, shape is {self.shape}")

        if not (0 <= col < self.shape[1]):
            raise IndexError(f"Column {col} out of range, shape is {self.shape}")

        for (row_range, col_range), pm in self.blocks.items():
            if row in row_range and col in col_range:
                block_row = row - row_range.start
                block_col = col - col_range.start
                return pm.at(block_row, block_col)

        # (row, col) is within bounds, but there is no block for this, so it is
        # filled with zeros
        return PolyDict.empty()


class SlicePolyMatrixMixin(PolyMatrixMixin, ABC):
    """ Slice of a poly matrix. """
    @property
    @abstractmethod
    def reference(self) -> PolyMatrixMixin:
        """ Polymatrix which the slice was taken from. """

    @property
    @abstractmethod
    def slice(self) -> tuple[tuple[int, ...], tuple[int, ...]]:
        """ Row and column indices to take from reference. """
        # TODO: consider changing this member to be of type
        #   tuple[slice | tuple[int, ...], slice | tuple[int, ...]]
        # so that one can optimize for storage, especially if you have huge
        # matrices and access 1:300. In the current implementation there would
        # be a tuple with 300 elements (bad for storage, though free fast to
        # access in at() method)

    @override
    def at(self, row: int, col: int) -> PolyDict:
        """ See :py:meth:`PolyMatrixMixin.at`. """
        # Check bounds
        if not (0 <= row < self.shape[0]):
            raise IndexError(f"Row {row} out of range, shape is {self.shape}")

        if not (0 <= col < self.shape[1]):
            raise IndexError(f"Column {col} out of range, shape is {self.shape}")

        ref_row = self.slice[0][row]
        ref_col = self.slice[1][col]

        return self.reference.at(ref_row, ref_col)


class PolyMatrixAsAffineExpressionMixin(PolyMatrixMixin, ABC):
    r"""
    Matrix with polynomial entries, stored as an expression that is affine in
    the monomials.

    For clarity we will show this through an example. Suppose :math:`x,y` are
    polynomial variables, and we have a matrix of polynomials

    .. math::
        P(x, y) = \begin{bmatrix}
            x + y^2  & x + 2\\
            2x^2 + xy + 1 & y
        \end{bmatrix}
        \in \mathbf{R}_2(x, y)^{2 \times 2}

    We can rewrite :math:`P(x,y)` as an affine expression in terms of
    the monomial basis generated by :math:`x` and :math:`y` and get

    .. math::

        P(x,y)
        = \begin{bmatrix} 0 & 2 \\ 1 & 0 \end{bmatrix}
        + \begin{bmatrix} 1 & 1 \\ 0 & 0 \end{bmatrix} x
        + \begin{bmatrix} 0 & 0 \\ 0 & 1 \end{bmatrix} y
        + \begin{bmatrix} 0 & 0 \\ 2 & 0 \end{bmatrix} x^2
        + \begin{bmatrix} 0 & 0 \\ 1 & 0 \end{bmatrix} xy
        + \begin{bmatrix} 1 & 0 \\ 0 & 0 \end{bmatrix} y^2

    More generally for a :math:`p \in \mathbf{R}_d(x)^{n\times m}, x \in
    \mathbf{R}^q` and the set of monomials of degree :math:`d` or lower
    :math:`\langle x \rangle_d = \{1, x_1, x_2, \ldots, x_q, x_1^2, x_1 x_2,
    \ldots, x_q^2, x_1^3, \ldots, x_q^d\}` by defining the set multi-indices of
    :math:`\langle x \rangle_d` to be 

    .. math::
        \mathcal{E} \langle x \rangle_d = \{
            (0, \ldots, 0), (1,0,\ldots,0), (0,1,\ldots,0), \ldots,
            (2,0,\ldots,0), \ldots, (0,\ldots,d)\}
            \subset \mathbf{Z}^d

    we can write
    
    .. math::
        p(x) = \sum_{\alpha \in \mathcal{E}\langle x \rangle_d} A_\alpha x^\alpha

    where each matrix :math:`A_\alpha \in \mathbf{R}^{n\times m}`. In the
    context of this class we will call the :math:`A_\alpha` matrices
    `affine coefficients`.

    **Note:** In the example above, all monomials powers upt to degree 2 are
    present, however in general some monomials may be missing. In that case
    this class does not store those as they are all zeros. See also the code of
    :py:meth:`affine_coefficient`.
    """

    # Type of matrix that stores affine coefficients
    MatrixType = csc_array

    @property
    @abstractmethod
    def affine_coefficients(self) -> MatrixType:
        r"""
        The big matrix that store all :math:`A_\alpha` matrices:

        .. math::
            A = \begin{bmatrix}
                A_{\alpha_1} & A_{\alpha_2} & \cdots & A_{\alpha_N}
            \end{bmatrix}
            \in
            \mathbf{R}^{n \times mN},
            \quad
            N \leq \sum_{k = 0}^d {q + k \choose k}
        """

    @property
    @abstractmethod
    def slices(self) -> dict[MonomialIndex, range]:
        r"""
        Map from monomial indices to column slices of the big matrix that
        stores all :math:`A_\alpha`.
        """

    @property
    @abstractmethod
    def degree(self) -> int:
        """ Maximal degree of the affine expressions """
        # Value of max(m.degree for m in self.slices.keys())
        # cached at construction


    @property
    @abstractmethod
    def variable_indices(self) -> tuple[int]:
        """ Indices of the variables involved in the expression, sorted. """

    @override
    def at(self, row: int, col: int) -> PolyDict:
        # Check bounds
        if not (0 <= row < self.shape[0]):
            raise IndexError(f"Row {row} out of range, shape is {self.shape}")

        if not (0 <= col < self.shape[1]):
            raise IndexError(f"Column {col} out of range, shape is {self.shape}")

        p = PolyDict.empty()
        for monomial in self.slices.keys():
            p[monomial] = self.affine_coefficient(monomial)[row, col]

        return p

    # -- Special methods for affine expression

    def monomials_eval(self, x: Sequence[float] | ArrayLike, monomials: Iterable[MonomialIndex]) -> dict[MonomialIndex, float]:
        x: NDArray[float] = np.array(x)
        if x.size != len(self.variable_indices):
            # TODO: improve error message, retrieve variable names from state?
            raise ValueError(f"The value must be a {len(self.variable_indices)} dimensional, however "
                             f"{x} has shape {x.shape}.")

        result: dict[MonomialIndex, float] = {}
        # To convert variable indices into positions in the x vector
        variable_to_x = {var: i for var, i in enumerate(self.variable_indices)}
        # Cache for values that have already been computed
        values: list[dict[int, float]] = [{1: v} for v in x]
        for monomial in monomials:
            result[monomial] = 1.
            for var in monomial:
                # Compute missing powers if necessary
                if var.power not in values[var.index]:
                    for k in range(max(values[var.index].keys()), var.power + 1):
                        values[var.index][k] = values[var.index][k - 1] * x[variable_to_x[var.index]]

                result[monomial] *= values[var.index][var.power]

        return result

    def monomials_eval_all(self, x: Sequence[float] | ArrayLike) -> dict[MonomialIndex, float]:
        r"""
        Evaluate all monomials up to degree :math:`d`.

        Given an :math:`x \in \mathbf{R}^q` this function computes the
        numerical values of :math:`\langle x \rangle_d = \{1, x_1, x_2, \ldots,
        x_q, x_1^2, x_1 x_2, \ldots, x_q^2, x_1^3, \ldots, x_q^d\}`.
        """

        # Number of variables in the expression
        q = len(self.variable_indices)

        x = np.array(x)
        if x.size != q:
            # TODO: improve error message, retrieve variable names from state?
            raise ValueError(f"The value must be a {q} dimensional, however "
                             f"{x} is {len(x)} dimensional.")

        def compute_with_triangles(d, x):
            # To compute all monomials we need to compute all integer points contained
            # in a d-dimensional triangle-like shape. For example given a q-dimensional
            # x = (x_1, x_2, ... x_q) to compute all quadratic terms (d = 2) we need to
            # evaluate all points in the following upper triangular part of the grid:
            #
            #              x1       x2      x3      ...     xq
            #          ┌───────────────────────────────────────┐
            #      x1  │ x1*x1    x1*x2    x1*x3    ...  x1*xq │
            #          │                                       │
            #      x2  │          x2*x2    x2*x3    ...  x2*xq │
            #          │                                       │
            #      x3  │                   x3*x3    ...  x3*xq │
            #          │                                       │
            #     ...  │                            ...   ...  │
            #          │                                       │
            #      xq  │                                 xq*xq │
            #          └───────────────────────────────────────┘
            #
            # For d = 2 we compute the products by iterating k from 1 to q and then
            # inside of that iterating j from k to q and multiply x[j] * x[k].
            #
            # For d = 3 the shape is an irregular tetrahedron, and so when we iterate k
            # from 1 to q then we must iterate over "triangular slices" of the
            # tetrahedron. Like for d = 2 where we take only values that have j > k,
            # for d = 3 we take all tetrahedron slices that are "higher" than level k.
            #
            # This pattern continues for d > 3 so we define a recursion over d by
            # defining the triangle function below, which returns all monomial
            # combinations of exactly degree d that are "higher" than j.
            #
            # To keep track of the values during the recursion we define a dictionary
            # where the key is a tuple with length smaller or equal to q that contains
            # the index of the variables that make up the product. For example:
            #
            #    x_1 <-> (0,)    x_1 * x_1       <-> (0, 0)
            #    x_2 <-> (1,)    x_2 * x_3       <-> (1, 2)
            #    x_3 <-> (2,)    x_1 * x_2 * x_2 <-> (0, 1, 1)
            #

            # Number variables
            q = len(x)

            # TODO: rewrite recursive code using dynamic programming table to
            # replace functools.cache
            @functools.cache
            def triangle(d, j):
                # Base case
                if d == 1:
                    return {
                        (i,): x[i]
                        for i in range(j, q)
                    }

                current = {}
                for k in range(j, q):
                    # Recursion! Iterate over the slices which are given by the
                    # d-1 case and are higher than k
                     for i, v in triangle(d-1, k).items():
                        current[i + (k,)] = v * x[k]

                return current

            # The monomials from degree 1 to d is the union of triangles with j = 0,
            # i.e. the union of all terms of exactly degree d
            terms = {}
            for i in range(1, d+1):
                terms |= triangle(i, 0)

            return terms

        # Add the constant term
        monomial_values = {
            MonomialIndex.constant(): 1.
        }

        # Compute all monomials and convert key to MonomialIndex
        for key, val in compute_with_triangles(self.degree, x).items():
            # See comment above for structure of key
            # Count occurrences to convert to MonomialIndex
            index = MonomialIndex(sorted(VariableIndex(self.variable_indices[k], v)
                                         for k, v in Counter(key).items()))
            monomial_values[index] = val
        
        # Sort the values by monomial index, which have a total order
        return dict(sorted(monomial_values.items()))

    def affine_coefficient(self, monomial: MonomialIndex | VariableIndex | int) -> MatrixType:
        r""" Get the affine coefficient :math:`A_\alpha` associated to :math:`x^\alpha`. """
        if isinstance(monomial, int):
            # interpret as linear
            monomial = VariableIndex(monomial, power=1)

        if isinstance(monomial, VariableIndex):
            monomial = MonomialIndex((monomial,))

        if monomial not in self.slices.keys():
            nrows, _ = self.shape
            return csc_array(self.shape)

        columns = self.slices[monomial]
        return self.affine_coefficients[:, tuple(columns)]

    def affine_coefficients_by_degrees(self, variables: Sequence[VariableIndex] | None = None) -> Iterable[tuple[int, MatrixType]]:
        """
        Iterate over the coefficients grouped by degree.
        """
        if not variables:
            # cast to tuple because it is used multiple times in the loop
            variables = tuple(VariableIndex(i, power=1) for i in self.variable_indices)

        else:
            # TODO: document behaviour on why it is allowed to pass variables
            # it has to do with the function
            # sumofsquares.problems.InternalSOSProblem.to_conic_problem
            variables = tuple(sorted(variables))

        for degree in range(self.degree +1):
            monomials = MonomialIndex.combinations_of_degree(variables, degree)
            columns = tuple(self.affine_coefficient(m) for m in monomials)
            yield degree, sparse.hstack(columns)

    def affine_eval(self, x: ArrayLike) -> NDArray:
        r"""
        Evaluate the affine expression
        :math:`p(x) = \sum_{\alpha \in \mathcal{E}\langle x \rangle_d} A_\alpha x^\alpha`
        at a given :math:`x`.

        The code performs the evaluation by computing the matrix product

        .. math::
            p(x) = \begin{bmatrix}
                A_{\alpha_1} & A_{\alpha_2} & \cdots & A_{\alpha_N}
            \end{bmatrix} (B(x) \otimes I_m)
            \quad

        where :math:`B(x)` contains the monomials and :math:`m` is the number
        of columns of each :math:`A_\alpha`. See also :py:meth:`monomials_eval`
        and :py:meth:`monomials_eval_all`.
        """
        x = np.array(x)

        # Get the number of variables involved
        q = len(self.variable_indices)

        if x.size != q:
            # TODO: improve error message, retrieve variable names from state?
            raise ValueError(f"The value must be a {q} dimensional, however "
                             f"{x} is {len(x)} dimensional.")

        # Compute number of monomials that are generated by that
        # TODO: this value should be cached
        n = sum(math.comb(k + q - 1, k) for k in range(0, self.degree+1))

        # Heuristic on efficiency, computing all monomials exploits the
        # structure to reduce the number of computations necessary. But the
        # efficiency gain is useless if most of the computed terms are not
        # used.
        # TODO: tune heuristic
        if len(self.slices) > 0:
            # Compute all powers of x up to degree d
            all_monomials = self.monomials_eval_all(x)
            monomials = {
                m: all_monomials[m]
                for m in self.slices.keys()
            }

        else:
            # Compute only the necessary terms
            monomials = self.monomials_eval(x, self.slices.keys())

        mvalues = np.array(tuple(monomials.values()))
        
        # Evaluate the affine expression
        _, ncols = self.shape
        return (self.affine_coefficients @ sparse.kron(mvalues, sparse.eye(ncols)).T).toarray()

    def affine_eval_fn(self) -> Callable[[MatrixType], MatrixType]:
        r"""
        Get a function that when called evaluates the expression
        :math:`p(x) = \sum_{\alpha \in \mathcal{E}\langle x \rangle_d} A_\alpha x^\alpha`
        at :math:`x`.
        """
        # TODO: If slow consider replacing with toolz.functoolz.curry from ctoolz
        return functools.partial(PolyMatrixAsAffineExpressionMixin.affine_eval, self)
