import abc

from polymatrix.polymatrix.mixins import PolyMatrixAsDictMixin


class PolyMatrix(PolyMatrixAsDictMixin, abc.ABC):
    # NP: Interface for matrix whose entries are multivariate polynomials
	pass
