from __future__ import annotations

import math

from typing import NamedTuple, Iterable, cast
from collections import UserDict
from itertools import product, dropwhile, combinations_with_replacement
from functools import reduce

# TODO: remove these types, they are here for backward compatiblity
MonomialData = tuple[tuple[int, int], ...]
PolynomialData = dict[MonomialData, int | float]
PolynomialMatrixData = dict[tuple[int, int], PolynomialData]


class VariableIndex(NamedTuple):
    r"""
    Index for a variable raised to an integral power.

    `VariableIndex` has a partial order with respect to the variable index. For
    example if :math:`x` had index 0, :math:`y` has index 1, then :math:`y
    \succeq x`, the exponent does not matter so :math:`y \succeq x^2`.
    """
    index: int # index in ExpressionState object
    power: int

    def __lt__(self, other):
        """ Variables indices can be sorted with respect to variable index. """
        return self.index < other.index

    def __str__(self) -> str:
        if self.power == 1:
            return f"V({self.index:d})"
        return f"V({self.index:d})^{self.power:d}"


class MonomialIndex(tuple[VariableIndex]):
    r"""
    Index for a monomial, i.e. a product of `VariableIndex`.

    `MonomialIndex` has a total order, they can be sorted with respect to the
    degree. If they have the same degree then they are sorted with respect to
    the index of the first variable.

    For example :math:`x^2 z \succeq xy` because the former has degree 3 while
    the latter is quadratic. Then :math:`yz \succeq xy` (assuming :math:`x` has
    index 0, :math:`y` has index 1 and :math:`z` 2) because :math:`y \succeq x`.

    Constant monomial indices are considered equal.
    """

    def __repr__(self):
        return "MonomialIndex(" + ",".join(repr(v) for v in self) + ")"

    def __str__(self):
        if len(self) == 0:
            return ""

        return " * ".join(str(v) for v in sorted(self))

    @property
    def degree(self) -> int:
        """ Degree of the monomial """
        if MonomialIndex.is_constant(self):
            return 0

        return sum(v.power for v in self)

    def __lt__(self, other):
        # Constants are equal
        if MonomialIndex.is_constant(self):
            return False

        # they have the same degree the index counts
        if self.degree == other.degree:
            # Assumes that monomialindex is sorted!
            def expand(m):
                for v in m:
                    for _ in range(v.power):
                        yield v

            def same_index(t):
                s, o = t
                return s.index == o.index
        
            s, o = next(dropwhile(same_index, zip(expand(self), expand(other))))
            return s < o

        return self.degree < other.degree

    # --- Helper / util static methods ---

    @staticmethod
    def empty() -> MonomialIndex:
        """ Get an empty monomial index. """
        return MonomialIndex(tuple())

    @staticmethod
    def constant() -> MonomialIndex:
        """ Get the placeholder for constant terms. """
        return MonomialIndex(tuple())

    @staticmethod
    def is_constant(index: MonomialIndex) -> bool:
        """ Returns true if it is indexing a constant monomial. """
        return len(index) == 0

    @staticmethod
    def sort(index: MonomialIndex) -> MonomialIndex:
        """ Sort the variable indices inside the monomial index. """
        return MonomialIndex(tuple(sorted(index)))

    @staticmethod
    def combinations_of_degree(variables: Iterable[VariableIndex], degree: int) -> Iterable[MonomialIndex]:
        """
        Compute the indices of all monomial combinations `variables` of exactly
        degree `degree`.
        """
        if degree == 0:
            yield MonomialIndex.constant()

        else:
            combinations = []
            for comb in combinations_with_replacement(variables, degree):
                # Convert variables into monomials and mutliply them
                monomials = (MonomialIndex((v,)) for v in comb)
                r = reduce(MonomialIndex.product, monomials, MonomialIndex.constant())
                combinations.append(r)

            yield from sorted(combinations)

    @staticmethod
    def product(left: MonomialIndex, right: MonomialIndex) -> MonomialIndex:
        """
        Compute the index of the product of two monomials.

        For example if left is the index of :math:`xy` and right is the index
        of :math:`y^2` this functions returns the index of :math:`xy^3`.
        """
        if MonomialIndex.is_constant(left):
            return right

        if MonomialIndex.is_constant(right):
            return left

        # Compute the product of each non-constant term in left with each
        # non-constant term in right, by using a dictionary {variable_index: power}
        result_dict: dict[int, int] = dict(left)
        for idx, power in right:
            if idx not in result_dict:
                result_dict[idx] = power
            else:
                result_dict[idx] += power

        result = MonomialIndex(VariableIndex(k, v) for k, v in result_dict.items())
        return MonomialIndex.sort(result)

    @staticmethod
    def differentiate(index: MonomialIndex, wrt: int) -> tuple[int, MonomialIndex | None]:
        """
        Differentiate a monomial with respect to the variable with index `wrt`.
        If `index` does not contain the variable `wrt` (the term disappears)
        returns None. The function also returns the degree of the monomial
        before it was differentiated.
        """
        old_pow, new_monom = None, []
        for v in index:
            if v.index == wrt:
                old_pow = v.power
                if old_pow > 1:
                    new_monom.append(VariableIndex(index=v.index,
                                                   power=old_pow - 1))
            else:
                new_monom.append(v)

        if old_pow is None:
            return 0, None
        
        return old_pow, MonomialIndex(tuple(new_monom))
        

class PolyDict(UserDict[MonomialIndex, int | float]):
    """ Polynomial, stored as a dictionary. """

    @property
    def degree(self) -> int:
        """ Degree of the polynomial. """
        return max(m.degree for m in self.monomials())

    @staticmethod
    def empty() -> PolyDict:
        return PolyDict({})

    def __repr__(self):
        return f"{self.__class__.__qualname__}({super().__repr__()})"

    def __str__(self) -> str:
        return " + ".join(f"{str(c)} * {str(m)}"
                          if len(m) > 0 else f"{str(c)}"
                          for m, c in self.items())

    def __setitem__(self, key: Iterable[VariableIndex] | MonomialIndex, value: int | float):
        if not isinstance(key, MonomialIndex):
            key = MonomialIndex(key)
        return super().__setitem__(key, value)

    def constant(self) -> int | float:
        """ Get the constant term of the polynomial. """
        if MonomialIndex.constant() in self:
            return self[MonomialIndex.constant()]
        return 0

    def is_constant(self) -> bool:
        """
        Check whether the polynomial is a constant polynomial. 
        """
        # polynomial is zero
        if len(self.keys()) == 0:
            return True

        if len(self.keys()) > 1:
            return False

        m, *_ = self.keys()
        return MonomialIndex.is_constant(m)

    def terms(self) -> Iterable[tuple[MonomialIndex, int | float]]:
        """ Iterate over terms with a non-zero coefficient. """
        # This is an alias for readability
        yield from self.items()

    def terms_of_degree(self, d: int) -> Iterable[tuple[MonomialIndex, int | float]]:
        """ Iterate over terms of degree `d` with a non-zero coefficient. """
        yield from ((m, coeff)
                    for m, coeff in self.terms()
                        if m.degree == d)

    def monomials(self) -> Iterable[MonomialIndex]:
        """ Get monomials that compose the polynomial. """
        # This is an alias for readability
        yield from self.keys()

    def coefficients(self) -> Iterable[int | float]:
        """ Get the coefficients of the polynomial. """
        # This is an alias for readability
        yield from self.values()

    # --- Helper / util static methods ---
    
    @staticmethod
    def product(left: PolyDict, right: PolyDict) -> PolyDict:
        """ Compute the product of two polynomials """
        # product of left and right
        p = PolyDict.empty()

        for (lm, lv), (rm, rv) in product(left.items(), right.items()):
            pv = lv * rv

            if math.isclose(pv, 0):
                continue

            pm = MonomialIndex.product(lm, rm)
            if pm not in p:
                p[pm] = pv

            elif not math.isclose(p[pm] + pv, 0):
                p[pm] += pv

        return p

    @staticmethod
    def differentiate(poly: PolyDict, wrt: int) -> PolyDict:
        """ Differentiate a varaible with respect to a variable """
        p = PolyDict.empty()
        for monomial, coeff in poly.terms():
            old_degree, m = MonomialIndex.differentiate(monomial, wrt)
            # term has disappeared
            if m is not None:
                p[m] = coeff * old_degree

        return p
            

class MatrixIndex(NamedTuple):
    """ Index to represent an entry of a matrix. """
    row: int
    col: int


class PolyMatrixDict(UserDict[MatrixIndex, PolyDict]):
    """ Matrix whose entries are polynomials, stored as a dictionary. """

    @staticmethod
    def empty() -> PolyMatrixDict:
        return PolyMatrixDict({})

    def __repr__(self):
        return f"{self.__class__.__qualname__}({super().__repr__()})"

    def __setitem__(self, key: tuple[int, int] | MatrixIndex, value: dict | PolyDict):
        if not isinstance(key, MatrixIndex):
            key = MatrixIndex(*key)

        if not isinstance(value, PolyDict):
            value = PolyDict(value)

        return super().__setitem__(key, value)

    def __getitem__(self, key: tuple[int, int] | MatrixIndex) -> PolyDict:
        return super().__getitem__(cast(MatrixIndex, key))

    def entries(self) -> Iterable[tuple[MatrixIndex, PolyDict]]:
        yield from self.items()


