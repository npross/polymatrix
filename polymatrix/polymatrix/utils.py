import itertools
import math

from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import MonomialData, MonomialIndex, PolyDict, PolyMatrixDict
from polymatrix.polymatrix.mixins import PolyMatrixMixin, PolyMatrixAsDictMixin
from polymatrix.utils.deprecation import deprecated


def pretty_format_polydict(p: PolyDict, state: ExpressionState) -> str:
    """ Pretty print a polynomial """
    strings = []
    for monomial, coeff in p.terms():
        s = f"{coeff}"
        for var in monomial:
            if var.power == 1:
                s += f" * {state.get_name(var.index)}"
            else:
                s += f" * {state.get_name(var.index)}^{var.power:d}"

        strings.append(s)
    return " + ".join(strings)


def pretty_format_polymatrixdict(p: PolyMatrixDict, shape: tuple[int, int], state: ExpressionState) -> str:
    """ Pretty print a polymatrix """
    nrows, ncols = shape
    columns = []
    for c in range(ncols):
        rows, row_width = [], 0
        for r in range(nrows):
            if (r, c) in p:
                entry = pretty_format_polydict(p[r, c], state)
                rows.append(entry)

                if len(entry) > row_width:
                    row_width = len(entry)
            else:
                rows.append("0")

        columns.append(tuple(row.rjust(row_width) for row in rows))

    string = ""
    for r in range(nrows):
        column = ",  ".join(columns[c][r] for c in range(ncols))
        string += f"[ {column} ]\n"

    return string


def pretty_format_polymatrix(p: PolyMatrixMixin, state: ExpressionState) -> str:
    """ Pretty print a polymatrix """
    if isinstance(p, PolyMatrixAsDictMixin):
        return pretty_format_polymatrixdict(p.data, (p.shape[0], p.shape[1]), state)

    else:
        # TODO: add pretty print for other functions
        raise NotImplementedError


# NP: this is a werid case of the product
# NP: merge is not intutive IMHO, find better name
@deprecated("Replaced by MonomialIndex.product")
def merge_monomial_indices(
    monomials: tuple[MonomialIndex, ...],
) -> MonomialIndex:
    """
    (x1**2 x2, x2**2)  ->  x1**2 x2**3

    or in terms of indices {x1: 0, x2: 1}:

        (
            ((0, 2), (1, 1)),      # x1**2 x2
            ((1, 2),)              # x2**2
        )  ->  ((0, 2), (1, 3))    # x1**1 x2**3
    """
    if len(monomials) == 0:
        return MonomialIndex.empty()

    elif len(monomials) == 1:
        return monomials[0]

    else:
        m1_dict = dict(monomials[0])

        for other in monomials[1:]:
            for index, count in other:
                if index in m1_dict:
                    m1_dict[index] += count
                else:
                    m1_dict[index] = count

        # sort monomials according to their index
        # ((1, 3), (0, 2))  ->  ((0, 2), (1, 3))
        return sort_monomial_indices(m1_dict.items())


# NP: compute index of product of polynomials
@deprecated("Replaced by PolyDict.product")
def multiply_polynomial(
    left: PolyDict,
    right: PolyDict,
    result: PolyDict,
) -> None:
    """
    Multiplies two polynomials `left` and `right` and adds the result to the mutable polynomial `result`.
    """

    for (left_monomial, left_value), (right_monomial, right_value) in itertools.product(
        left.items(), right.items()
    ):
        value = left_value * right_value

        if math.isclose(value, 0, abs_tol=1e-12):
            continue

        monomial = merge_monomial_indices((left_monomial, right_monomial))

        if monomial not in result:
            result[monomial] = 0

        result[monomial] += value

        if math.isclose(result[monomial], 0, abs_tol=1e-12):
            del result[monomial]


# NP: sort mononial indices with respect to variable index in state object
@deprecated("With new index types you can use sorted() on MonomialIndex")
def sort_monomial_indices(
    monomial: MonomialData,
) -> MonomialData:
    return tuple(
        sorted(
            monomial,
            key=lambda m: m[0],
        )
    )

# NP: Sort list of monomials according to ... what?
def sort_monomials(
    monomials: tuple[MonomialData],
) -> tuple[MonomialData]:
    return tuple(
        sorted(
            monomials,
            key=lambda m: (sum(count for _, count in m), len(m), m),
        )
    )


# NP: what does this function do? split according to what?
def split_monomial_indices(
    monomial: MonomialData,
) -> tuple[MonomialData, MonomialData]:
    left = []
    right = []

    is_left = True

    for idx, count in monomial:
        count_left = count // 2

        if count % 2:
            if is_left:
                count_left = count_left + 1

            is_left = not is_left

        count_right = count - count_left

        if 0 < count_left:
            left.append((idx, count_left))

        if 0 < count_right:
            right.append((idx, count - count_left))

    return tuple(left), tuple(right)

# NP: consider making a module containing all exceptions
class SubtractError(Exception):
    pass


# NP: set difference / division of polynomials?
# NP: name is very confusing, find better name once is clear what it does
def subtract_monomial_indices(
    m1: MonomialData,
    m2: MonomialData,
) -> MonomialData:
    m1_dict = dict(m1)

    for index, count in m2:
        if index not in m1_dict:
            raise SubtractError()

        m1_dict[index] -= count

        if m1_dict[index] == 0:
            del m1_dict[index]

        elif m1_dict[index] < 0:
            raise SubtractError()

    return sort_monomial_indices(m1_dict.items())
