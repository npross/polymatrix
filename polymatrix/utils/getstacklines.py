import dataclasses
import traceback


@dataclasses.dataclass(frozen=True)
class FrameSummary:
    filename: str
    lineno: int
    name: str
    line: str


def get_stack_lines(index: int = 2) -> tuple[FrameSummary]:
    def gen_stack_lines():
        # NP: according to the stdlib traceback documentation these objects
        # NP: should not be left around indefintely as they can cause memory leaks
        for obj in traceback.extract_stack()[:-index]:
            yield FrameSummary(
                filename=obj.filename,
                lineno=obj.lineno,
                name=obj.name,
                line=obj.line,
            )

    return tuple(gen_stack_lines())
