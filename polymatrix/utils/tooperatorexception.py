from polymatrix.utils.getstacklines import FrameSummary


def to_operator_exception(
    message: str,
    stack: tuple[FrameSummary],
) -> str:
    exception_lines = [
        message,
        f"  Assertion traceback (most recent call last):",
        *(
            f'    File "{stack_line.filename}", line {stack_line.lineno}\n      {stack_line.line}'
            for stack_line in stack
        ),
    ]

    return "\n".join(exception_lines)
