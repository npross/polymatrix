import polymatrix.expression.init

from polymatrix.utils.getstacklines import get_stack_lines
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin

# NP: IIRC this file is redundant and will be removed, soon~ish?
# TODO: remove this file in favour of operations directly via Expression objects

def diff(
    expression: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    introduce_derivatives: bool = None,
) -> ExpressionBaseMixin:
    if not isinstance(variables, ExpressionBaseMixin):
        variables = polymatrix.expression.init.init_from_expr(variables)

    if introduce_derivatives is None:
        introduce_derivatives = False

    return polymatrix.expression.impl.DerivativeExprImpl(
        underlying=expression,
        variables=variables,
        introduce_derivatives=introduce_derivatives,
        stack=get_stack_lines(),
    )


def filter_(
    underlying: ExpressionBaseMixin,
    predicator: ExpressionBaseMixin,
    inverse: bool = None,
) -> ExpressionBaseMixin:
    if inverse is None:
        inverse = False

    return polymatrix.expression.impl.FilterExprImpl(
        underlying=underlying,
        predicator=predicator,
        inverse=inverse,
    )


def integrate(
    expression: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    from_: tuple[float, ...],
    to: tuple[float, ...],
) -> ExpressionBaseMixin:
    if not isinstance(variables, ExpressionBaseMixin):
        variables = polymatrix.expression.init.init_from_expr(variables)

    assert len(from_) == len(to)

    return polymatrix.expression.impl.IntegrateExprImpl(
        underlying=expression,
        variables=variables,
        from_=from_,
        to=to,
        stack=get_stack_lines(),
    )


def legendre(
    expression: ExpressionBaseMixin,
    degrees: tuple[int, ...] = None,
) -> ExpressionBaseMixin:
    return polymatrix.expression.impl.LegendreSeriesImpl(
        underlying=expression,
        degrees=degrees,
        stack=get_stack_lines(),
    )


def linear_in(
    expression: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    monomials: ExpressionBaseMixin = None,
    ignore_unmatched: bool = None,
) -> ExpressionBaseMixin:
    if monomials is None:
        monomials = linear_monomials(
            expression=expression,
            variables=variables,
        )

    return polymatrix.expression.impl.LinearInExprImpl(
        underlying=expression,
        monomials=monomials,
        variables=variables,
        ignore_unmatched=ignore_unmatched,
    )


def linear_monomials(
    expression: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
) -> ExpressionBaseMixin:
    return polymatrix.expression.impl.LinearMonomialsExprImpl(
        underlying=expression,
        variables=variables,
    )


def degree(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.DegreeExprImpl(
        underlying=underlying,
        stack=get_stack_lines(),
    )
