import math
import sympy
import numpy as np

from polymatrix.expression.expression import Expression
from polymatrix.expressionstate import ExpressionState
from polymatrix.statemonad import StateMonad, init_state_monad


def shape(
    expr: Expression,
) -> StateMonad[ExpressionState, tuple[int, ...]]:
    def func(state: ExpressionState):
        state, polymatrix = expr.apply(state)

        return state, polymatrix.shape

    return init_state_monad(func)


def to_constant(
    expr: Expression,
    assert_constant: bool = True,
) -> StateMonad[ExpressionState, np.ndarray]:
    def func(state: ExpressionState):
        state, underlying = expr.apply(state)

        A = np.zeros(underlying.shape, dtype=np.double)

        for (row, col), polynomial in underlying.gen_data():
            for monomial, value in polynomial.items():
                if len(monomial) == 0:
                    A[row, col] = value

                elif assert_constant:
                    raise Exception(f"non-constant term {monomial=}")

        return state, A

    return init_state_monad(func)


def to_sympy(
    expr: Expression,
) -> StateMonad[ExpressionState, sympy.Expr | sympy.Matrix]:

    def polymatrix_to_sympy(state: ExpressionState) -> tuple[ExpressionState, sympy.Expr | sympy.Matrix]:

        # Convert to polymatrix
        state, pm = expr.apply(state)

        m = sympy.zeros(*pm.shape)
        for entry, poly in pm.entries():
            sympy_poly_terms = []
            for monomial, coeff in poly.terms():
                sympy_monomial = math.prod(
                        sympy.Symbol(state.get_name(variable.index)) ** variable.power
                        for variable in monomial)

                if math.isclose(coeff, 1.):
                    # no need to add 1 in front
                    sympy_poly_terms.append(sympy_monomial)

                else:
                    sympy_poly_terms.append(coeff * sympy_monomial)

            m[entry] = sum(sympy_poly_terms)

        if math.prod(pm.shape) == 1:
            # just return the expression
            return state, m[0, 0]
        
        return state, m
    return init_state_monad(polymatrix_to_sympy, expr)
