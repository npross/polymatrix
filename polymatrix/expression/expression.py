from __future__ import annotations

import dataclasses
import numpy as np

from numpy.typing import ArrayLike
from abc import ABC, abstractmethod
from dataclassabc import dataclassabc
from typing import Iterable
from typing_extensions import override, overload


import polymatrix.expression.init as init

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.variableexprmixin import VariableExprMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.utils.getstacklines import get_stack_lines

from polymatrix.expression.op import (
    diff,
    integrate,
    linear_in,
    linear_monomials,
    legendre,
    filter_,
    degree,
)

class Expression(ExpressionBaseMixin, ABC):
    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        # NP: I know what it is but it was very confusing in the beginning.
        # FIXME: provide documentation on how underlying works / what it means
        ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        return self.underlying.apply(state)

    def read(self, state: ExpressionState) -> PolyMatrix:
        return self.apply(state)[1]

    def __add__(self, other: ExpressionBaseMixin) -> Expression:
        return self._binary(init.init_addition_expr, self, other)

    def __getattr__(self, name):
        attr = getattr(self.underlying, name)

        if isinstance(attr, ExpressionBaseMixin):
            return dataclasses.replace(
                self,
                underlying=attr,
            )

        else:
            return attr

    def __getitem__(self, item: int | slice | tuple[int | slice, int | slice]) -> Expression:
        return self.copy(
            underlying=init.init_slice_expr(
                underlying=self.underlying,
                slices=item
            ),
        )

    def __matmul__(self, other: ExpressionBaseMixin | np.ndarray) -> Expression:
        return self._binary(
            init.init_matrix_mult_expr, self, other
        )

    def __mul__(self, other) -> Expression:
        return self._binary(init.init_elem_mult_expr, self, other)

    def __pow__(self, exponent: Expression | int | float) -> Expression:
        return self._binary(init.init_power_expr, self, exponent)

    def __neg__(self):
        return init_expression(init.init_negation_expr(self.underlying))

    def __radd__(self, other):
        return self._binary(init.init_addition_expr, other, self)

    def __rmatmul__(self, other):
        return self._binary(init.init_matrix_mult_expr, other, self)

    def __rmul__(self, other):
        return self._binary(init.init_elem_mult_expr, other, self)

    def __rsub__(self, other):
        return self._binary(init.init_subtraction_expr, other, self)

    def __sub__(self, other):
        return self._binary(init.init_subtraction_expr, self, other)

    def __truediv__(self, other):
        if not isinstance(other, float | int):
            return NotImplemented
        
        return (1 / other) * self

    @abstractmethod
    def copy(self, underlying: ExpressionBaseMixin) -> "Expression": ...

    @staticmethod
    def _binary(op, left, right):
        stack = get_stack_lines()

        if isinstance(left, Expression) and isinstance(right, Expression):
            return left.copy(underlying=op(left.underlying, right.underlying, stack))

        elif isinstance(left, Expression):
            right = init.init_from_expr_or_none(right)

            if right is None:
                return NotImplemented

            return left.copy(underlying=op(left.underlying, right, stack))

        # else right is an Expression
        else:
            left = init.init_from_expr_or_none(left)

            if left is None:
                return NotImplemented

            return right.copy(underlying=op(left, right.underlying, stack))

    def cache(self) -> Expression:
        return self.copy(
            underlying=init.init_cache_expr(
                underlying=self.underlying,
            ),
        )

    def combinations(self, degrees: tuple[int, ...] | int | Expression):
        if isinstance(degrees, Expression):
            degrees = degrees.underlying

        return self.copy(
            underlying=init.init_combinations_expr(
                expression=self.underlying,
                degrees=degrees,
            ),
        )

    def degree(self) -> Expression:
        return self.copy(
            underlying=degree(
                underlying=self.underlying,
            ),
        )

    def determinant(self) -> Expression:
        return self.copy(
            underlying=init.init_determinant_expr(
                underlying=self.underlying,
            ),
        )

    def diag(self):
        return self.copy(
            underlying=init.init_diag_expr(
                underlying=self.underlying,
            ),
        )

    # FIXME: sometime variables is a tuple, sometimes an expression. make consistent.
    # FIXME: this function is probably broken
    def diff(self, variables: Expression, introduce_derivatives: bool | None = None) -> Expression:
        return self.copy(
            underlying=diff(
                expression=self.underlying,
                variables=variables.underlying,
                introduce_derivatives=introduce_derivatives,
            ),
        )

    def divergence(self, variables: tuple) -> Expression:
        return self.copy(
            underlying=init.init_divergence_expr(
                underlying=self.underlying,
                variables=variables,
            ),
        )

    def hessian(self, variables: Expression) -> Expression:
        return self.copy(
            underlying=init.init_hessian_expr(
                underlying=self.underlying,
                variables=variables
            )
        )

    @overload
    def eval(self, variables: Iterable[VariableExpression], values: Iterable[float | ArrayLike]) -> Expression:
        """
        Evaluate some variables at the given values.

        Given a list of variables and values (that must have the same length),
        replace all occurrences of each variable with the given value.
        """

    @overload
    def eval(self, variables: dict[VariableExpression, float | ArrayLike]) -> Expression:
        """
        Evaluate some variables at the given values.

        Given a dictionary (map) from variables to values, replace all occurrences
        of the variable with the value.
        """

    def eval(self, variables, values = None) -> Expression:
        if values is not None:
            # FIXME: some of utils.formatsubstitutions.formatsubstitutions behaviour was removed
            #        and needs to be restored
            if len(variables) != len(values):
                raise ValueError("The number of values does not match the number of variables to replace! "
                                 f"{len(variables)} variables and {len(values)} were given.")
            it = zip(variables, values)
        elif isinstance(variables, dict):
            it = variables.items()
        else:
            raise TypeError("Invalid type {type(variables)} for variables.")

        substitutions = []
        for v, val in it:
            # Because of how the cache works val needs to be hashable
            if isinstance(val, np.ndarray):
                val = tuple(val.reshape(-1))

            elif isinstance(val, int | float):
                val = (val,)

            substitutions.append((v.var.symbol, val))

        return self.copy(
            underlying=init.init_eval_expr(
                underlying=self.underlying,
                substitutions=tuple(substitutions)
            ),
        )

    # also applies to monomials (and variables?)
    def filter(
        self,
        predicator: Expression,
        inverse: bool | None = None,
    ) -> Expression:
        return self.copy(
            underlying=filter_(
                underlying=self.underlying,
                predicator=predicator,
                inverse=inverse,
            ),
        )

    # only applies to symmetric matrix
    def from_symmetric_matrix(self) -> Expression:
        return self.copy(
            underlying=init.init_from_symmetric_matrix_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to monomials
    def half_newton_polytope(self, variables: Expression, filter: Expression | None = None,) -> Expression:
        return self.copy(
            underlying=init.init_half_newton_polytope_expr(
                monomials=self.underlying,
                variables=variables,
                filter=filter,
            ),
        )

    def integrate(
        self,
        variables: Expression,
        from_: tuple[float, ...],
        to: tuple[float, ...]
    ) -> Expression:
        return self.copy(
            underlying=integrate(
                expression=self,
                variables=variables,
                from_=from_,
                to=to,
            ),
        )

    def linear_matrix_in(self, variable: Expression) -> Expression:
        return self.copy(
            underlying=init.init_linear_matrix_in_expr(
                underlying=self.underlying,
                variable=variable,
            ),
        )

    def linear_monomials(self, variables: Expression) -> Expression:
        return self.copy(
            underlying=linear_monomials(
                expression=self.underlying,
                variables=variables,
            ),
        )

    def linear_in(
        self,
        variables: Expression,
        monomials: Expression | None = None,
        ignore_unmatched: bool | None = None,
    ) -> Expression:
        return self.copy(
            underlying=linear_in(
                expression=self.underlying,
                monomials=monomials,
                variables=variables,
                ignore_unmatched=ignore_unmatched,
            ),
        )

    def legendre(self, degrees: tuple[int, ...] | None = None) -> Expression:
        return self.copy(
            underlying=legendre(
                expression=self.underlying,
                degrees=degrees,
            ),
        )

    def max(self) -> Expression:
        return self.copy(
            underlying=init.init_max_expr(
                underlying=self.underlying,
            ),
        )

    def parametrize(self, name: str | None = None) -> Expression:
        return self.copy(
            underlying=init.init_parametrize_expr(
                underlying=self.underlying,
                name=name,
            ),
        )

    def quadratic_in(self, variables: Expression, monomials: Expression | None = None) -> Expression:
        if monomials is None:
            monomials = self.quadratic_monomials(variables)

        stack = get_stack_lines()

        return self.copy(
            underlying=init.init_quadratic_in_expr(
                underlying=self.underlying,
                monomials=monomials,
                variables=variables,
                stack=stack,
            ),
        )

    def quadratic_monomials(
        self,
        variables: "Expression",
    ) -> "Expression":
        return self.copy(
            underlying=init.init_quadratic_monomials_expr(
                underlying=self.underlying,
                variables=variables,
            ),
        )

    def reshape(self, n: int, m: int) -> "Expression":
        return self.copy(
            underlying=init.init_reshape_expr(
                underlying=self.underlying,
                new_shape=(n, m),
            ),
        )

    def rep_mat(self, n: int, m: int) -> "Expression":
        return self.copy(
            underlying=init.init_rep_mat_expr(
                underlying=self.underlying,
                repetition=(n, m),
            ),
        )

    # FIXME: replace with __setitem__?
    def set_element_at(
        self,
        row: int,
        col: int,
        value: "Expression",
    ) -> "Expression":
        if isinstance(value, Expression):
            value = value.underlying
        else:
            value = init.init_from_expr(value)

        return self.copy(
            underlying=init.init_set_element_at_expr(
                underlying=self.underlying,
                index=(row, col),
                value=value,
            ),
        )

    @property
    def shape(self) -> Expression:
        return self.copy(underlying=init.init_shape_expr(self.underlying))

    # remove?
    def squeeze(
        self,
    ) -> "Expression":
        return self.copy(
            underlying=init.init_squeeze_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to monomials
    def subtract_monomials(
        self,
        monomials: "Expression",
    ) -> "Expression":
        return self.copy(
            underlying=init.init_subtract_monomials_expr(
                underlying=self.underlying,
                monomials=monomials,
            ),
        )

    def sum(self):
        return self.copy(
            underlying=init.init_sum_expr(
                underlying=self.underlying,
            ),
        )

    def symmetric(self):
        return self.copy(
            underlying=init.init_symmetric_expr(
                underlying=self.underlying,
            ),
        )

    def transpose(self) -> "Expression":
        return self.copy(
            underlying=init.init_transpose_expr(
                underlying=self.underlying,
            ),
        )

    @property
    def T(self) -> "Expression":
        return self.transpose()

    def to_constant(self) -> "Expression":
        return self.copy(
            underlying=init.init_to_constant_expr(
                underlying=self.underlying,
            ),
        )

    def to_symmetric_matrix(self) -> "Expression":
        return self.copy(
            underlying=init.init_to_symmetric_matrix_expr(
                underlying=self.underlying,
            ),
        )

    # only applies to variables
    def to_sorted_variables(self) -> "Expression":
        return self.copy(
            underlying=init.init_to_sorted_variables(
                underlying=self.underlying,
            ),
        )

    # also applies to monomials?
    def truncate(
        self,
        degrees: tuple[int],
        variables: tuple | None = None,
        inverse: bool = None,
    ):
        return self.copy(
            underlying=init.init_truncate_expr(
                underlying=self.underlying,
                variables=variables,
                degrees=degrees,
                inverse=inverse,
            ),
        )


@dataclassabc(frozen=True)
class ExpressionImpl(Expression):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"Expression({self.underlying})"

    def copy(self, underlying: ExpressionBaseMixin) -> Expression:
        return dataclasses.replace(
            self,
            underlying=underlying,
        )


def init_expression(
    underlying: ExpressionBaseMixin,
):
    return ExpressionImpl(
        underlying=underlying,
    )


class VariableExpression(Expression):
    """ 
    Expression that is a polynomial variable, i.e. an expression that cannot be
    reduced further.
    """
    @property
    def var(self):
        return self.underlying

    @override
    @property
    def shape(self):
        return self.underlyng.shape


@dataclassabc(frozen=True)
class VariableExpressionImpl(VariableExpression):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"VariableExpression({self.underlying})"

    def copy(self, underlying: ExpressionBaseMixin) -> Expression:
        return init_expression(underlying)


def init_variable_expression(underlying: VariableExprMixin):
    return VariableExpressionImpl(underlying)

