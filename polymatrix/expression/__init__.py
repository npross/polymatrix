from collections.abc import Iterable
from typing import Callable
from functools import wraps

import polymatrix.expression.from_ as from_
import polymatrix.expression.impl

from polymatrix.utils.getstacklines import get_stack_lines
from polymatrix.expression.expression import init_expression, Expression
from polymatrix.expression.from_ import from_any
from polymatrix.expression.mixins.namedexprmixin import NamedExprMixin

from polymatrix.expression.init import (
    init_arange_expr,
    init_block_diag_expr,
    init_concatenate_expr,
    init_lower_triangular_expr,
    init_named_expr,
    init_ns_expr,
)

def convert_args_to_expression(fn: Callable) -> Callable:
    @wraps(fn)
    def wrapper(*args, **kwargs):
        wrapped_args = (
            from_.from_any(arg)
            for arg in args
        )

        wrapped_kwargs = {
            kw: from_.from_any(arg)
            for kw, arg in kwargs.items()
        }

        return fn(*wrapped_args, **wrapped_kwargs)
    return wrapper


def ones(shape: int | tuple[int | Expression, int | Expression] | Expression) -> Expression:
    """ Make a matrix filled with ones """
    if isinstance(shape, Expression):
        expr = init_ns_expr(n=1, shape=shape.underlying)

    elif isinstance(shape, int):
        expr = init_ns_expr(n=1, shape=v_stack((shape, 1)))

    elif isinstance(shape, tuple):
        nrows, ncols = shape
        if (not isinstance(nrows, int)) or (not isinstance(ncols, int)):
            shape = from_any(((nrows,), (ncols,))).underlying

        expr = init_ns_expr(n=1, shape=shape)

    else:
        # TODO: error messages
        raise TypeError

    return init_expression(expr)


@convert_args_to_expression
def arange(start_or_stop: Expression, stop: Expression | None = None, step: Expression | None = None):
    # Replicate range()'s behaviour
    if stop is None and step is None:
        e = init_arange_expr(start=None, stop=start_or_stop.underlying, step=None)

    elif stop is not None and step is None:
        e = init_arange_expr(start=start_or_stop.underlying, stop=stop.underlying, step=None)

    elif stop is not None and step is not None:
        e = init_arange_expr(start=start_or_stop.underlying, stop=stop.underlying, step=step.underlying)

    else:
        # FIXME: error message missing
        raise ValueError

    return init_expression(e)


def zeros(shape: int | tuple[int | Expression, int | Expression] | Expression):
    """ Make a matrix filled with zeros """
    if isinstance(shape, Expression):
        expr = init_ns_expr(n=0, shape=shape.underlying)

    elif isinstance(shape, int):
        expr = init_ns_expr(n=0, shape=v_stack((shape, 1)))

    elif isinstance(shape, tuple):
        nrows, ncols = shape
        if (not isinstance(nrows, int)) or (not isinstance(ncols, int)):
            shape = from_any(((nrows,), (ncols,))).underlying

        expr = init_ns_expr(n=0, shape=shape)

    else:
        # TODO: error messages
        raise TypeError

    return init_expression(expr)


def v_stack(expressions: Iterable) -> Expression:
    """ Vertically stack expressions """
    # arrange blocks vertically and concatanete
    blocks = tuple((from_.from_any(e).underlying,) for e in expressions)
    u = init_expression(init_concatenate_expr(blocks))
    names = ", ".join(str(b[0]) for b in blocks)
    return give_name(u, f"vstack({names})")


def h_stack(expressions: Iterable) -> Expression:
    """ Horizontally stack expressions """
    # arrange horizontally
    blocks = (tuple(from_.from_any(e).underlying for e in expressions),)
    u = init_expression(init_concatenate_expr(blocks))
    names = ", ".join(str(b) for b in blocks[0])
    return give_name(u, f"hstack({names})")


def concatenate(expressions: Iterable[Iterable]):
    """ Concatenate arrays (more general version of vstack and hstack) """
    blocks = tuple(tuple(from_.from_any(expr).underlying for expr in row) for row in expressions)
    return init_expression(init_concatenate_expr(blocks))


def block_diag(expressions: Iterable[Expression]) -> Expression:
    """ Create a block diagonal matrix. """
    return init_expression(init_block_diag_expr(tuple(e.underlying for e in expressions)))


def product(
    expressions: Iterable[Expression],
    degrees: tuple[int, ...] | None = None,
):
    return init_expression(
        underlying=polymatrix.expression.impl.ProductExprImpl(
            underlying=tuple(expressions),
            degrees=degrees,
            stack=get_stack_lines(),
        )
    )


@convert_args_to_expression
def lower_triangular(vector: Expression):
    return init_expression(init_lower_triangular_expr(vector.underlying))


def give_name(expr: Expression, name: str):
    """ Give a name to an expression. """
    return init_expression(init_named_expr(expr.underlying, name))


def what_is(expr: Expression) -> Expression:
    """
    Opposite of :py:fn:`give_name`. Query what is inside a named expression.
    Example:
    .. py:

        p = poly.give_name(x ** 2 + 1, "p(x)")
        print(p) # shows "p(x)"
        print(poly.what_is(p)) # shows "(x ** 2) + 1"
    """

    if not isinstance(expr.underlying, NamedExprMixin):
        raise TypeError(f"{expr} is not a named expression!")

    return init_expression(expr.underlying.underlying)
