
import sympy
import numpy

from numpy.typing import NDArray
from typing import Iterable, Any

import polymatrix.expression.init as init

from polymatrix.expression.init import init_variable_expr
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.variableexprmixin import VariableExprMixin
from polymatrix.expression.typing import FromSupportedTypes
from polymatrix.expressionstate import ExpressionState
from polymatrix.statemonad import StateMonad
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.utils.deprecation import deprecated
from polymatrix.symbol import Symbol

from polymatrix.expression.expression import (
    init_expression, Expression,
    init_variable_expression, VariableExpression
)


def from_any(value: FromSupportedTypes) -> Expression:
    """
    Attempt create an expression object from a value. Raises an exception if
    the expression cannot be constructed from given value.
    """
    if v := from_any_or(value, None):
        return v

    raise ValueError("Unsupported type. Cannot construct expression "
                    f"from value {value} with type {type(value)}")


def from_any_or(value: FromSupportedTypes, value_if_not_supported: Any) -> Expression | Any:
    """
    Create an expression object from a value, or give value_if_not_supported if
    the expression cannot be constructed from the given value.
    """
    if isinstance(value, VariableExpression | Expression):
        return value

    elif isinstance(value, ExpressionBaseMixin):
        return init_expression(value)

    elif isinstance(value, int | float):
        return from_number(value)

    elif isinstance(value, StateMonad):
        return from_state_monad(value)

    elif isinstance(value, numpy.ndarray):
        return from_numpy(value)

    elif isinstance(value, sympy.Matrix | sympy.Expr):
        return from_sympy(value)

    elif isinstance(value, tuple):
        if len(value) < 1:
            return value_if_not_supported

        from polymatrix.expression import v_stack, h_stack

        # matrix given as tuple[tuple[...]], row major order
        if all(isinstance(row, tuple) for row in value):
            wrapped_rows: list[list[Expression]] = []
            for row in value:
                if len(row) != len(value[0]):
                    return value_if_not_supported

                wrapped_row: list[Expression] = []
                for col in row:
                    wrapped = from_any_or(col, None)
                    if wrapped is None:
                        return value_if_not_supported
                    wrapped_row.append(wrapped)

                wrapped_rows.append(wrapped_row)
            return v_stack(h_stack(row) for row in wrapped_rows)
                           

        # row vector tuple[...]
        elif all(not isinstance(row, tuple) for row in value):
            wrapped_rows: list[Expression] = []
            for row in value:
                wrapped = from_any_or(row, None)
                if wrapped is None:
                    return value_if_not_supported
                wrapped_rows.append(wrapped)

            return h_stack(wrapped_rows)

        # invalid
        else:
            value_if_not_supported

    return value_if_not_supported


def from_names(
    names: str,
    shape: Expression | tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin] | ExpressionBaseMixin = (1,1)
) -> Iterable[VariableExpression]:
    """ Construct one or multiple variables from comma separated a list of names. """
    for name in names.split(","):
        yield from_name(name.strip(), shape)


def from_name(
    name: str, 
    shape: Expression | tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin] | ExpressionBaseMixin = (1,1)
) -> VariableExpression:
    """ Construct a variable from its names """
    if isinstance(shape, Expression):
        shape = shape.underlying

    elif isinstance(shape, tuple):
        nrows, ncols = shape
        if (not isinstance(nrows, int)) or (not isinstance(ncols, int)):
            shape = from_any(((nrows,), (ncols,))).underlying

    return init_variable_expression(init.init_variable_expr(Symbol(name), shape))


def from_number(num: int | float) -> Expression:
    """ Construct an expression from a number. """
    return init_expression(init.init_from_numbers_expr(((num,),)))


def from_numbers(nums: Iterable[int | float] | Iterable[Iterable[int | float]]) -> Expression:
    """ Construct vector or matrix from numbers. """
    numbers = tuple(nums)

    # Row vector
    if isinstance(numbers[0], int | float):
        return init_expression(init.init_from_numbers_expr((tuple(numbers),)))

    # Matrix
    numbers = tuple(tuple(n) for row in numbers for n in row)
    return init_expression(init.init_from_numbers_expr(numbers))


def from_numpy(array: NDArray) -> Expression:
    """ Convert a Numpy Array into an Expression. """
    return init_expression(init.init_from_numpy_expr(array))


def from_state_monad(monad: StateMonad[ExpressionState, ExpressionBaseMixin | PolyMatrixMixin]) -> Expression:
    """
    Create an expression from a StateMonad that returns an expression or
    polymatrix.
    """
    return init_expression(init.init_from_statemonad(monad))


def from_sympy(expr: sympy.Matrix | sympy.Expr | tuple[tuple[sympy.Expr, ...], ...]) -> Expression:
    """ Convert a sympy expression into an Expression. """
    return init_expression(init.init_from_sympy_expr(expr))


# -- Old API --

@deprecated(from_any_or)
def from_expr_or_none(data: FromSupportedTypes) -> Expression | None:
    return from_any_or(data, None)


@deprecated(from_any)
def from_(*args, **kwargs) -> Expression:
    return from_any(*args, **kwargs)
