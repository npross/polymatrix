import collections
import dataclasses
import itertools

from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolynomialData


# NP: why does this return a dict?
# NP: why doe we need considered_variables and introduce_derivatives? Wouldn't
# NP: be better to do partial NP: derivatives and the define total derivative in
# NP: terms of partial derivative?
# NP:
# NP: this is a recursive function, consider making it more clear by using the
# NP: structure (=> Where is the code that makes sure that the recursion stops?)
# NP:
# NP:   def recursive_fn(args):
# NP:       if base_case:
# NP:           return base_case_result
# NP:
# NP:       # compute new args for recursion
# NP:       result = recursive_fn(new_args)
# NP:       # compute complete result after recursion or do tail call recursion
# NP:       return complete_result
# NP:
# NP: if recursion is more complex split into smaller functions
# FIXME: typing
def differentiate_polynomial(
    polynomial: PolynomialData,
    diff_wrt_variable: int,
    state: ExpressionState,
    considered_variables: set,
    introduce_derivatives: bool,
):
    @dataclasses.dataclass(frozen=True)
    class DerivativeKey:
        variable: int
        with_respect_to: int

    if introduce_derivatives:

        def gen_new_variables():
            for monomial in polynomial.keys():
                for var in monomial:
                    if var is not diff_wrt_variable and var not in considered_variables:
                        yield var

        new_variables = set(gen_new_variables())

        new_considered_variables = considered_variables | new_variables

        def acc_state_candidates(acc, new_variable):
            state, candidates = acc

            # key representing the derivative of the variable
            key = DerivativeKey(
                variable=new_variable,
                with_respect_to=diff_wrt_variable,
            )
            # register if it doesn't already exist
            state = state.register(key=key, n_param=1)

            # for each new variable we expect an auxillary equation
            state, auxillary_derivation_terms = differentiate_polynomial(
                polynomial=state.auxillary_equations[new_variable],
                diff_wrt_variable=diff_wrt_variable,
                state=state,
                considered_variables=new_considered_variables,
                introduce_derivatives=True,
            )

            if 1 < len(auxillary_derivation_terms):
                derivation_variable = state.offset_dict[key][0]

                state = dataclasses.replace(
                    state,
                    auxillary_equations=state.auxillary_equations
                    | {derivation_variable: auxillary_derivation_terms},
                )

                return state, candidates + (new_variable,)

            else:
                return state, candidates

        *_, (state, confirmed_variables) = itertools.accumulate(
            new_variables,
            acc_state_candidates,
            initial=(state, tuple()),
        )

    else:
        confirmed_variables = tuple()

    diff_polynomial = collections.defaultdict(float)

    for monomial, value in polynomial.items():
        monomial_cnt = dict(monomial)

        def differentiate_monomial(dependent_variable, derivation_variable=None):
            def gen_diff_monomial():
                for current_variable, current_count in monomial:
                    if current_variable is dependent_variable:
                        sel_counter = current_count - 1

                    else:
                        sel_counter = current_count

                    # for _ in range(sel_counter):
                    #     yield current_variable
                    if 0 < sel_counter:
                        yield current_variable, sel_counter

                if derivation_variable is not None:
                    yield derivation_variable

            diff_monomial = tuple(sorted(gen_diff_monomial()))

            return diff_monomial, value * monomial_cnt[dependent_variable]

        if diff_wrt_variable in monomial_cnt:
            diff_monomial, value = differentiate_monomial(diff_wrt_variable)
            diff_polynomial[diff_monomial] += value

        # only used if introduce_derivatives == True
        if introduce_derivatives:
            for candidate_variable in monomial_cnt.keys():
                if (
                    candidate_variable in considered_variables
                    or candidate_variable in confirmed_variables
                ):
                    key = DerivativeKey(
                        variable=candidate_variable,
                        with_respect_to=diff_wrt_variable,
                    )
                    derivation_variable = state.offset_dict[key][0]

                    diff_monomial, value = differentiate_monomial(
                        dependent_variable=candidate_variable,
                        derivation_variable=derivation_variable,
                    )
                    diff_polynomial[diff_monomial] += value

    return state, dict(diff_polynomial)
    # return state, derivation_terms
