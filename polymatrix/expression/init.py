import typing
import numpy as np
import numpy.typing as npt
import sympy
from typing import Any, Iterable

from polymatrix.expression.typing import FromSupportedTypes 

import polymatrix.expression.impl

from polymatrix.expression.expression import VariableExpression
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.index import PolynomialMatrixData
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.statemonad import StateMonad
from polymatrix.symbol import Symbol
from polymatrix.utils.getstacklines import FrameSummary


def init_addition_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.AdditionExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_arange_expr(
    start: int | ExpressionBaseMixin | None,
    stop: int | ExpressionBaseMixin,
    step: int | ExpressionBaseMixin | None
):
    return polymatrix.expression.impl.ARangeExprImpl(start, stop, step)


def init_block_diag_expr(blocks: tuple[ExpressionBaseMixin, ...]):
    return polymatrix.expression.impl.BlockDiagExprImpl(blocks)


def init_cache_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.CacheExprImpl(underlying=underlying)


def init_combinations_expr(
    expression: ExpressionBaseMixin,
    degrees: tuple[int, ...] | int | ExpressionBaseMixin,
):
    if isinstance(degrees, int):
        degrees = (degrees,)

    return polymatrix.expression.impl.CombinationsExprImpl(
        expression=expression,
        degrees=degrees,
    )


def init_concatenate_expr(blocks: tuple[tuple[ExpressionBaseMixin, ...], ...]):
    return polymatrix.expression.impl.ConcatenateExprImpl(blocks)


def init_diag_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.DiagExprImpl(
        underlying=underlying,
    )


def init_divergence_expr(underlying: ExpressionBaseMixin, variables: tuple):
    return polymatrix.expression.impl.DivergenceExprImpl(
        underlying=underlying,
        variables=variables,
    )

def init_elem_mult_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.ElemMultExprImpl(
        left=left,
        right=right,
    )


# FIXME: typing
def init_eval_expr(
    underlying: ExpressionBaseMixin,
    substitutions: Iterable[tuple[Symbol, Any]]
):
    return polymatrix.expression.impl.EvalExprImpl(
        underlying=underlying,
        substitutions=substitutions,
    )


# FIXME: make it take a shape not a variable
def init_eye_expr(variable: ExpressionBaseMixin):
    return polymatrix.expression.impl.EyeExprImpl(
        variable=variable,
    )


def init_from_symmetric_matrix_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.FromSymmetricMatrixExprImpl(
        underlying=underlying,
    )


def init_from_numbers_expr(data: tuple[tuple[int | float]]):
    return polymatrix.expression.impl.FromNumbersExprImpl(data=data)


def init_from_numpy_expr(data: npt.NDArray):
    return polymatrix.expression.impl.FromNumpyExprImpl(data=data)


def init_from_sympy_expr(data: sympy.Expr | sympy.Matrix | tuple[tuple[sympy.Expr, ...], ...]):
    return polymatrix.expression.impl.FromSympyExprImpl(data=data)


def init_from_statemonad(monad: StateMonad):
    return polymatrix.expression.impl.FromStateMonadImpl(monad=monad)


# TODO: remove this function, replaced by from_any
def init_from_expr_or_none(
    data: FromSupportedTypes,
) -> ExpressionBaseMixin | None:
    if isinstance(data, VariableExpression):
        return data

    if isinstance(data, str):
        return init_parametrize_expr(
            underlying=init_from_expr_or_none(1), # FIXME: typing
            name=data,
        )

    elif isinstance(data, int | float):
        # Wrap in a tuple of tuples
        wrapped = ((data,),)
        return init_from_numbers_expr(wrapped)

    elif isinstance(data, StateMonad):
        return init_from_statemonad(data)

    elif isinstance(data, np.ndarray):
        return init_from_numpy_expr(data)

    elif isinstance(data, sympy.Expr | sympy.Matrix):
        return init_from_sympy_expr(data)

    if isinstance(data, tuple):
        if len(data) < 1:
            return None

        if isinstance(data[0], tuple):
            if len(data[0]) < 1:
                return None

            if isinstance(data[0][0], sympy.Expr):
                return init_from_sympy_expr(data)

            elif isinstance(data[0][0], int | float):
                return init_from_numbers_expr(data)

        elif isinstance(data[0], sympy.Expr):
            return init_from_sympy_expr((data,))

    elif isinstance(data, ExpressionBaseMixin):
        return data

    return None


def init_from_expr(data: FromSupportedTypes):
    expr = init_from_expr_or_none(data)

    if expr is None:
        raise Exception(f"{data=}")

    return expr


# FIXME: typing
def init_from_terms_expr(
    data: PolyMatrixMixin | PolynomialMatrixData,
    shape: tuple[int, int] = None,
):
    if isinstance(data, PolyMatrixMixin):
        shape = data.shape # FIXME: typing, also cf. PolyMatrixMixin.shape() comment
        poly_matrix_data = data.gen_data()

    else:
        assert shape is not None

        if isinstance(data, tuple):
            poly_matrix_data = data

        elif isinstance(data, dict):
            poly_matrix_data = data.items() # FIXME: typing

        else:
            raise Exception(f"{data=}")

    # Expression needs to be hashable
    data_as_tuple = tuple(
        (coord, tuple(polynomial.items())) for coord, polynomial in poly_matrix_data
    )

    return polymatrix.expression.impl.FromPolynomialDataExprImpl(
        terms=data_as_tuple,
        shape=shape,
    )


def init_half_newton_polytope_expr(
    monomials: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    filter: ExpressionBaseMixin | None = None,
):
    return polymatrix.expression.impl.HalfNewtonPolytopeExprImpl(
        monomials=monomials, variables=variables, filter=filter
    )


def init_hessian_expr(
    underlying: ExpressionBaseMixin,
    variables: ExpressionBaseMixin
):
    return polymatrix.expression.impl.HessianExprImpl(
            underlying=underlying, variables=variables)


def init_linear_matrix_in_expr(
    underlying: ExpressionBaseMixin,
    variable: int,
):
    # FIXME: LinearMatrixInExprImpl has an abstract method variables and cannot
    # be instantiated
    return polymatrix.expression.impl.LinearMatrixInExprImpl(
        underlying=underlying,
        variable=variable,
    )


def init_lower_triangular_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.LowerTriangularExprImpl(underlying)


def init_matrix_mult_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.MatrixMultExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_max_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.MaxExprImpl(
        underlying=underlying)


def init_named_expr(underlying: ExpressionBaseMixin, name: str):
    return polymatrix.expression.impl.NamedExprImpl(
            underlying=underlying, name=name)


def init_negation_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.NegationExprImpl(
            underlying=underlying)


def init_ns_expr(n: int | float | ExpressionBaseMixin, shape: tuple[int, int] | ExpressionBaseMixin):
    return polymatrix.expression.impl.NsExprImpl(n, shape)


def init_parametrize_expr(
    underlying: ExpressionBaseMixin,
    name: str = None, # FIXME: typing
):
    if name is None:
        name = "undefined"

    return polymatrix.expression.impl.ParametrizeExprImpl(
        underlying=underlying,
        name=name,
    )


def init_power_expr(
    left: ExpressionBaseMixin, 
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary]
):
    return polymatrix.expression.impl.PowerExprImpl(left=left, right=right)


def init_quadratic_in_expr(
    underlying: ExpressionBaseMixin,
    monomials: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    assert isinstance(variables, ExpressionBaseMixin), f"{variables=}"

    return polymatrix.expression.impl.QuadraticInExprImpl(
        underlying=underlying,
        monomials=monomials,
        variables=variables, # FIXME: typing
        stack=stack,
    )


def init_quadratic_monomials_expr(
    underlying: ExpressionBaseMixin,
    variables: ExpressionBaseMixin,
):
    assert isinstance(variables, ExpressionBaseMixin), f"{variables=}"

    return polymatrix.expression.impl.QuadraticMonomialsExprImpl(
        underlying=underlying,
        variables=variables, # FIXME: typing
    )


def init_rep_mat_expr(
    underlying: ExpressionBaseMixin,
    repetition: tuple,
):
    return polymatrix.expression.impl.RepMatExprImpl(
        underlying=underlying,
        repetition=repetition,
    )


def init_reshape_expr(
    underlying: ExpressionBaseMixin,
    new_shape: tuple,
):
    return polymatrix.expression.impl.ReshapeExprImpl(
        underlying=underlying,
        new_shape=new_shape,
    )


def init_set_element_at_expr(
    underlying: ExpressionBaseMixin,
    index: tuple,
    value: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SetElementAtExprImpl(
        underlying=underlying,
        index=index,
        value=value,
    )


def init_shape_expr(underlying: ExpressionBaseMixin):
    return polymatrix.expression.impl.ShapeExprImpl(underlying)


def init_slice_expr(
    underlying: ExpressionBaseMixin,
    slices: int | slice | range | tuple[int | slice | range, int | slice | range]
):
    # FIXME: see comment above this HashableSlice class
    HashableSlice = polymatrix.expression.mixins.sliceexprmixin.HashableSlice

    if isinstance(slices, slice):
        slices = HashableSlice(slices.start, slices.stop, slices.step)

    elif isinstance(slices, tuple):
        new_slices = list(slices)
        if isinstance(slices[0], slice):
            new_slices[0] = HashableSlice(slices[0].start, slices[0].stop, slices[0].step)

        if isinstance(slices[1], slice):
            new_slices[1] = HashableSlice(slices[1].start, slices[1].stop, slices[1].step)

        slices = tuple(new_slices)

    return polymatrix.expression.impl.SliceExprImpl(underlying, slices)



def init_squeeze_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SqueezeExprImpl(
        underlying=underlying,
    )

def init_subtraction_expr(
    left: ExpressionBaseMixin,
    right: ExpressionBaseMixin,
    stack: tuple[FrameSummary],
):
    return polymatrix.expression.impl.SubtractionExprImpl(
        left=left,
        right=right,
        stack=stack,
    )


def init_subtract_monomials_expr(
    underlying: ExpressionBaseMixin,
    monomials: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SubtractMonomialsExprImpl(
        underlying=underlying,
        monomials=monomials,
    )


def init_sum_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SumExprImpl(
        underlying=underlying,
    )


def init_symmetric_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.SymmetricExprImpl(
        underlying=underlying,
    )


def init_to_constant_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToConstantExprImpl(
        underlying=underlying,
    )


def init_to_quadratic_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToQuadraticExprImpl(
        underlying=underlying,
    )


def init_to_sorted_variables(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToSortedVariablesImpl(
        underlying=underlying,
    )


def init_to_symmetric_matrix_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.ToSymmetricMatrixExprImpl(
        underlying=underlying,
    )


def init_transpose_expr(
    underlying: ExpressionBaseMixin,
):
    return polymatrix.expression.impl.TransposeExprImpl(
        underlying=underlying,
    )


def init_truncate_expr(
    underlying: ExpressionBaseMixin,
    degrees: tuple[int],
    variables: ExpressionBaseMixin | None = None,
    inverse: bool | None = None,
):
    if isinstance(degrees, int):
        degrees = (degrees,)

    if inverse is None:
        inverse = False

    # FIME: typing
    return polymatrix.expression.impl.TruncateExprImpl(
        underlying=underlying,
        variables=variables,
        degrees=degrees,
        inverse=inverse,
    )


def init_variable_expr(sym: Symbol, shape: tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin] | ExpressionBaseMixin):
    return polymatrix.expression.impl.VariableExprImpl(sym, shape)
