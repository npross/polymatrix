import numpy.typing
import sympy
from typing import Iterable
from typing_extensions import override
from polymatrix.statemonad import StateMonad

import dataclassabc

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.symbol import Symbol

from polymatrix.expression.mixins.additionexprmixin import AdditionExprMixin
from polymatrix.expression.mixins.arangeexprmixin import ARangeExprMixin
from polymatrix.expression.mixins.blockdiagexprmixin import BlockDiagExprMixin
from polymatrix.expression.mixins.cacheexprmixin import CacheExprMixin
from polymatrix.expression.mixins.combinationsexprmixin import CombinationsExprMixin
from polymatrix.expression.mixins.concatenateexprmixin import ConcatenateExprMixin
from polymatrix.expression.mixins.degreeexprmixin import DegreeExprMixin
from polymatrix.expression.mixins.derivativeexprmixin import DerivativeExprMixin
from polymatrix.expression.mixins.diagexprmixin import DiagExprMixin
from polymatrix.expression.mixins.divergenceexprmixin import DivergenceExprMixin
from polymatrix.expression.mixins.elemmultexprmixin import ElemMultExprMixin
from polymatrix.expression.mixins.evalexprmixin import EvalExprMixin
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.eyeexprmixin import EyeExprMixin
from polymatrix.expression.mixins.filterexprmixin import FilterExprMixin
from polymatrix.expression.mixins.fromnumbersexprmixin import FromNumbersExprMixin
from polymatrix.expression.mixins.fromnumpyexprmixin import FromNumpyExprMixin
from polymatrix.expression.mixins.fromstatemonad import FromStateMonadMixin
from polymatrix.expression.mixins.fromsymmetricmatrixexprmixin import FromSymmetricMatrixExprMixin
from polymatrix.expression.mixins.fromsympyexprmixin import FromSympyExprMixin
from polymatrix.expression.mixins.halfnewtonpolytopeexprmixin import HalfNewtonPolytopeExprMixin
from polymatrix.expression.mixins.hessianexprmixin import HessianExprMixin
from polymatrix.expression.mixins.integrateexprmixin import IntegrateExprMixin
from polymatrix.expression.mixins.legendreseriesmixin import LegendreSeriesMixin
from polymatrix.expression.mixins.linearinexprmixin import LinearInExprMixin
from polymatrix.expression.mixins.linearmatrixinexprmixin import LinearMatrixInExprMixin
from polymatrix.expression.mixins.linearmonomialsexprmixin import LinearMonomialsExprMixin
from polymatrix.expression.mixins.lowertriangularexprmixin import LowerTriangularExprMixin
from polymatrix.expression.mixins.matrixmultexprmixin import MatrixMultExprMixin
from polymatrix.expression.mixins.maxexprmixin import MaxExprMixin
from polymatrix.expression.mixins.namedexprmixin import NamedExprMixin
from polymatrix.expression.mixins.negationexprmixin import NegationExprMixin
from polymatrix.expression.mixins.nsexprmixin import NsExprMixin
from polymatrix.expression.mixins.parametrizeexprmixin import ParametrizeExprMixin
from polymatrix.expression.mixins.powerexprmixin import PowerExprMixin
from polymatrix.expression.mixins.productexprmixin import ProductExprMixin
from polymatrix.expression.mixins.quadraticinexprmixin import QuadraticInExprMixin
from polymatrix.expression.mixins.quadraticmonomialsexprmixin import QuadraticMonomialsExprMixin
from polymatrix.expression.mixins.repmatexprmixin import RepMatExprMixin
from polymatrix.expression.mixins.reshapeexprmixin import ReshapeExprMixin
from polymatrix.expression.mixins.setelementatexprmixin import SetElementAtExprMixin
from polymatrix.expression.mixins.shapeexprmixin import ShapeExprMixin
from polymatrix.expression.mixins.sliceexprmixin import SliceExprMixin
from polymatrix.expression.mixins.squeezeexprmixin import SqueezeExprMixin
from polymatrix.expression.mixins.subtractionexprmixin import SubtractionExprMixin
from polymatrix.expression.mixins.subtractmonomialsexprmixin import SubtractMonomialsExprMixin
from polymatrix.expression.mixins.sumexprmixin import SumExprMixin
from polymatrix.expression.mixins.symmetricexprmixin import SymmetricExprMixin
from polymatrix.expression.mixins.toconstantexprmixin import ToConstantExprMixin
from polymatrix.expression.mixins.tosortedvariablesmixin import ToSortedVariablesExprMixin
from polymatrix.expression.mixins.tosymmetricmatrixexprmixin import ToSymmetricMatrixExprMixin
from polymatrix.expression.mixins.transposeexprmixin import TransposeExprMixin
from polymatrix.expression.mixins.truncateexprmixin import TruncateExprMixin
from polymatrix.expression.mixins.variableexprmixin import VariableExprMixin

from polymatrix.expression.mixins.fromtermsexprmixin import (
    FromPolynomialDataExprMixin,
    PolynomialMatrixTupledData,
)


@dataclassabc.dataclassabc(frozen=True)
class AdditionExprImpl(AdditionExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(left={repr(self.left)}, right={repr(self.right)})"

    def __str__(self):
        return f"({self.left} + {self.right})"


@dataclassabc.dataclassabc(frozen=True)
class ARangeExprImpl(ARangeExprMixin):
    start: int | ExpressionBaseMixin | None
    stop: int | ExpressionBaseMixin
    step: int | ExpressionBaseMixin | None

    def __str__(self):
        if self.start:
            if self.step:
                return f"arange({self.start}, {self.stop}, {self.end})"
            else:
                return f"arange({self.start}, {self.stop})"
        else:
            if self.step:
                return f"arange(0, {self.stop}, {self.step})"
            else:
                return f"arange({self.stop})"


@dataclassabc.dataclassabc(frozen=True)
class BlockDiagExprImpl(BlockDiagExprMixin):
    blocks: tuple[ExpressionBaseMixin, ...]

    def __str__(self):
        return f"blkdiag({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class CacheExprImpl(CacheExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class CombinationsExprImpl(CombinationsExprMixin):
    expression: ExpressionBaseMixin
    degrees: ExpressionBaseMixin | tuple[int, ...]

    def __str__(self):
        if isinstance(self.degrees, tuple) and len(self.degrees) == 1:
            return f"combinations({self.expression}, {self.degrees[0]})"
        return f"combinations({self.expression}, {self.degrees})"


@dataclassabc.dataclassabc(frozen=True)
class ConcatenateExprImpl(ConcatenateExprMixin):
    blocks: tuple[tuple[ExpressionBaseMixin, ...], ...]

    def __str__(self):
        blocks = ", ".join(
            "(" + ", ".join(str(b) for b in row) + ")"
                for row in self.blocks)

        return f"cat({blocks})"


@dataclassabc.dataclassabc(frozen=True)
class DerivativeExprImpl(DerivativeExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    introduce_derivatives: bool
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(variables={self.variables}, underlying={repr(self.underlying)})"

    def __str__(self):
        return f"(∂_{self.variables} {self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class DiagExprImpl(DiagExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"diag({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class DivergenceExprImpl(DivergenceExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple


@dataclassabc.dataclassabc(frozen=True)
class ElemMultExprImpl(ElemMultExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin

    def __str__(self):
        return f"({self.left} * {self.right})"


@dataclassabc.dataclassabc(frozen=True)
class EvalExprImpl(EvalExprMixin):
    underlying: ExpressionBaseMixin
    substitutions: Iterable[tuple]

    def __str__(self):
        return f"eval({self.underlying}, {self.substitutions})"


@dataclassabc.dataclassabc(frozen=True)
class EyeExprImpl(EyeExprMixin):
    variable: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class FilterExprImpl(FilterExprMixin):
    underlying: ExpressionBaseMixin
    predicator: ExpressionBaseMixin
    inverse: bool


@dataclassabc.dataclassabc(frozen=True)
class FromNumbersExprImpl(FromNumbersExprMixin):
    data: tuple[tuple[int | float]]

    def __str__(self):
        if len(self.data) == 1:
            if len(self.data[0]) == 1:
                return str(self.data[0][0])

        return str(self.data)


@dataclassabc.dataclassabc(frozen=True)
class FromNumpyExprImpl(FromNumpyExprMixin):
    data: numpy.typing.NDArray

    def __str__(self):
        return f"from_numpy({self.data})"


@dataclassabc.dataclassabc(frozen=True)
class FromStateMonadImpl(FromStateMonadMixin):
    monad: StateMonad

    def __str__(self):
        return f"from_statemonad({self.monad})"


@dataclassabc.dataclassabc(frozen=True)
class FromSympyExprImpl(FromSympyExprMixin):
    data: sympy.Expr | sympy.Matrix | tuple[tuple[sympy.Expr]]

    def __str__(self):
        return f"from_sympy({self.data})"


@dataclassabc.dataclassabc(frozen=True)
class FromSymmetricMatrixExprImpl(FromSymmetricMatrixExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class FromPolynomialDataExprImpl(FromPolynomialDataExprMixin):
    data: PolynomialMatrixTupledData
    shape: tuple[int, int]


@dataclassabc.dataclassabc(frozen=True)
class HalfNewtonPolytopeExprImpl(HalfNewtonPolytopeExprMixin):
    monomials: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    filter: ExpressionBaseMixin | None

    def __str__(self):
        # FIXME: what does filter do?
        return f"halfnewton({self.monomials}, {self.variables})"


@dataclassabc.dataclassabc(frozen=True)
class HessianExprImpl(HessianExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin

    def __str__(self):
        return f"(∂²_{self.variables} {self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class IntegrateExprImpl(IntegrateExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    from_: tuple[float, ...]
    to: tuple[float, ...]
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, variables={self.variables}, from_={self.from_}, to={self.to})"


@dataclassabc.dataclassabc(frozen=True)
class LinearInExprImpl(LinearInExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    ignore_unmatched: bool

    def __str__(self):
        return f"linear_in({self.underlying}, {self.monomials})"


@dataclassabc.dataclassabc(frozen=True)
class LinearMatrixInExprImpl(LinearMatrixInExprMixin):
    underlying: ExpressionBaseMixin
    variable: int


@dataclassabc.dataclassabc(frozen=True)
class LinearMonomialsExprImpl(LinearMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple

    def __str__(self):
        return f"linear_monomials({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class LowerTriangularExprImpl(LowerTriangularExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"tril({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class LegendreSeriesImpl(LegendreSeriesMixin):
    underlying: ExpressionBaseMixin
    degrees: tuple[int, ...] | None
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, degrees={self.degrees})"


@dataclassabc.dataclassabc(frozen=True)
class MatrixMultExprImpl(MatrixMultExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    # FIXME: remove this?
    def __repr__(self):
        return f"{self.__class__.__name__}(left={self.left}, right={self.right})"

    def __str__(self):
        return f"({self.left} @ {self.right})"


@dataclassabc.dataclassabc(frozen=True)
class DegreeExprImpl(DegreeExprMixin):
    underlying: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying})"

    def __str__(self):
        return f"degree({str(self.underlying)})"


@dataclassabc.dataclassabc(frozen=True)
class MaxExprImpl(MaxExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"max({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class NamedExprImpl(NamedExprMixin):
    underlying: ExpressionBaseMixin
    name: str

    def __str__(self):
        return str(self.name)


@dataclassabc.dataclassabc(frozen=True)
class NegationExprImpl(NegationExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"(-{self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class NsExprImpl(NsExprMixin):
    n: int | float | ExpressionBaseMixin
    shape: tuple[int, int] | ExpressionBaseMixin

    def __str__(self):
        return f"({self.n}s)"


@dataclassabc.dataclassabc(frozen=True, repr=False)
class ParametrizeExprImpl(ParametrizeExprMixin):
    underlying: ExpressionBaseMixin
    name: str

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(name={self.name}, underlying={self.underlying})"
        )


@dataclassabc.dataclassabc(frozen=True)
class PowerExprImpl(PowerExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin | int | float

    def __str__(self):
        return f"({self.left} ** {self.right})"


@dataclassabc.dataclassabc(frozen=True)
class ProductExprImpl(ProductExprMixin):
    underlying: tuple[ExpressionBaseMixin]
    degrees: tuple[int, ...] | None
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(underlying={self.underlying}, degrees={self.degrees})"


@dataclassabc.dataclassabc(frozen=True)
class QuadraticInExprImpl(QuadraticInExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin
    variables: tuple # FIXME: typing
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(variables={self.variables}, monomials={self.monomials}, underlying={repr(self.underlying)})"


    def __str__(self):
        return f"quadratic_in({str(self.underlying)}, {str(self.variables)})"


@dataclassabc.dataclassabc(frozen=True)
class QuadraticMonomialsExprImpl(QuadraticMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    variables: tuple

    def __str__(self):
        return f"quadratic_monomials({str(self.underlying)})"


@dataclassabc.dataclassabc(frozen=True)
class RepMatExprImpl(RepMatExprMixin):
    underlying: ExpressionBaseMixin
    repetition: tuple


@dataclassabc.dataclassabc(frozen=True)
class ReshapeExprImpl(ReshapeExprMixin):
    underlying: ExpressionBaseMixin
    new_shape: tuple


@dataclassabc.dataclassabc(frozen=True)
class SetElementAtExprImpl(SetElementAtExprMixin):
    underlying: ExpressionBaseMixin
    index: tuple
    value: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ShapeExprImpl(ShapeExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"shape({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class SliceExprImpl(SliceExprMixin):
    underlying: ExpressionBaseMixin
    slice: tuple # See SlicePolyMatrixMixin 

    def __str__(self):
        return f"{self.underlying}[{self.slice}]"


@dataclassabc.dataclassabc(frozen=True)
class SqueezeExprImpl(SqueezeExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SubtractionExprImpl(SubtractionExprMixin):
    left: ExpressionBaseMixin
    right: ExpressionBaseMixin
    stack: tuple[FrameSummary]

    # implement custom __repr__ method that returns a representation without the stack
    def __repr__(self):
        return f"{self.__class__.__name__}(left={repr(self.left)}, right={repr(self.right)})"

    def __str__(self):
        return f"({self.left} - {self.right})"


@dataclassabc.dataclassabc(frozen=True)
class SubtractMonomialsExprImpl(SubtractMonomialsExprMixin):
    underlying: ExpressionBaseMixin
    monomials: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class SumExprImpl(SumExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"(Σ {self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class SymmetricExprImpl(SymmetricExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"symmetric({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class ToConstantExprImpl(ToConstantExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"ToCostant({self.underlying})"


@dataclassabc.dataclassabc(frozen=True)
class ToSortedVariablesImpl(ToSortedVariablesExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class ToSymmetricMatrixExprImpl(ToSymmetricMatrixExprMixin):
    underlying: ExpressionBaseMixin


@dataclassabc.dataclassabc(frozen=True)
class TransposeExprImpl(TransposeExprMixin):
    underlying: ExpressionBaseMixin

    def __str__(self):
        return f"{self.underlying}.T"


@dataclassabc.dataclassabc(frozen=True)
class TruncateExprImpl(TruncateExprMixin):
    underlying: ExpressionBaseMixin
    variables: ExpressionBaseMixin
    degrees: tuple[int]
    inverse: bool


@dataclassabc.dataclassabc(frozen=True)
class VariableExprImpl(VariableExprMixin):
    symbol: Symbol
    shape: tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin] | ExpressionBaseMixin

    def __str__(self):
        return self.symbol
