import abc
import itertools
import dataclassabc
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyDict, MonomialIndex


class EyeExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def variable(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, variable = self.variable.apply(state)

        # FIXME: move to polymatrix module
        @dataclassabc.dataclassabc(frozen=True)
        class EyePolyMatrix(PolyMatrixMixin):
            shape: tuple[int, int]

            def at(self, row: int, col: int) -> PolyDict:
                size, _ = self.shape
                if max(row, col) > size:
                    raise IndexError(f"Identity matrix has size {size}, {row, col} is out of bounds.")

                if row != col:
                    return PolyDict.empty()

                return PolyDict({MonomialIndex.constant(): 1.})

        # FIXME: this behaviour is counterintuitive, eye should take just a number for the dimension
        n_row = variable.shape[0]

        polymatrix = EyePolyMatrix(
            shape=(n_row, n_row),
        )

        return state, polymatrix
