from __future__ import annotations

import itertools
from abc import  abstractmethod
from dataclasses import replace
from typing_extensions import override

from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MonomialIndex, VariableIndex
from polymatrix.symbol import Symbol


class VariableExprMixin(ExpressionBaseMixin):
    """ Underlying object for VariableExpression """

    @property
    @abstractmethod
    def shape(self) -> tuple[int | ExpressionBaseMixin, int | ExpressionBaseMixin] | ExpressionBaseMixin:
        """ Shape of the variable expression. """

    @property
    @abstractmethod
    def symbol(self) -> Symbol:
        """ The symbol representing the variable. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        if isinstance(self.shape, ExpressionBaseMixin):
            state, shape_pm = self.shape.apply(state)
            if shape_pm.shape != (2, 1):
                raise ValueError("If shape is an expression it must evaluate to a 2d row vector, "
                                 f"but here it has shape {shape_pm.shape}")

            # FIXME: should check that they are actually integers
            nrows = int(shape_pm.at(0, 0).constant())
            ncols = int(shape_pm.at(1, 0).constant())

        elif isinstance(self.shape, tuple):
            if isinstance(self.shape[0], ExpressionBaseMixin):
                state, nrows_pm = self.shape[0].apply(state)
                nrows = int(nrows_pm.scalar().constant())

            elif isinstance(self.shape[0], int):
                nrows = self.shape[0]

            else:
                raise TypeError("Number of row in shape must be either an integer, "
                    "or an expression that evaluates to a scalar integer. "
                    f"But given value has type {type(self.shape[0])}")

            if isinstance(self.shape[1], ExpressionBaseMixin):
                state, ncols_pm = self.shape[1].apply(state)
                ncols = int(ncols_pm.scalar().constant())

            elif isinstance(self.shape[1], int):
                ncols = self.shape[1]
                
            else:
                raise TypeError("Number of columns in shape must be either an integer, "
                    "or a expression that evaluates to a scalar integer. "
                    f"But given value has type {type(self.shape[0])}")

        else:
            raise TypeError("Shape must be a tuple or expression that "
                f"evaluates to a 2d row vector, cannot be of type {type(self.shape)}")

        state = state.register(self.symbol, shape=(nrows, ncols)) 
        indices = state.get_indices(self.symbol)

        p = PolyMatrixDict()
        for (row, col), index in zip(itertools.product(range(nrows), range(ncols)), indices):
            p[row, col] = PolyDict({
                # Create monomial with variable to the first power
                # with coefficient of one
                MonomialIndex((VariableIndex(index, power=1),)): 1.
            })

        pm = init_poly_matrix(p, shape=(nrows, ncols))
        return replace(state, cache=state.cache | {self: p}), pm
