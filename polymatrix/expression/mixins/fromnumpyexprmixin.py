import math

from abc import abstractmethod
from typing_extensions import override, cast
from numpy.typing import NDArray

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState

from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.init import init_poly_matrix 
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MonomialIndex


class FromNumpyExprMixin(ExpressionBaseMixin):
    """
    Make a (constant) expression from a numpy array.

    ..code:: py
        identity = polymatrix.from_numpy(np.eye(3))
    """

    @property
    @abstractmethod
    def data(self) -> NDArray:
        """ The Numpy array. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        p = PolyMatrixDict.empty()

        if len(self.data.shape) > 2:
            raise ValueError("Cannot construct expression from numpy array with "
                             f"shape {self.data.shape}, only vectors and matrices are allowed")

        # Case when it is a (n,) array
        data = self.data
        if len(data.shape) == 1:
            data = data.reshape(self.data.shape[0], 1)

        nrows, ncols = data.shape
        for r in range(nrows):
            for c in range(ncols):
                if not math.isclose(self.data[r, c], 0):
                    p[r, c] = PolyDict({
                        MonomialIndex.constant(): data[r, c]
                    })

        return state, init_poly_matrix(p, (nrows, ncols))
