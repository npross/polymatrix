import abc
import dataclassabc

from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.index import PolyDict
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState


class TransposeExprMixin(ExpressionBaseMixin):
    """
    Transpose the polynomial matrix

    [[1, 2, 3]]  ->  [[1], [2], [3]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `PolyMatrixExprBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        # FIXME: move to polymatrix module
        @dataclassabc.dataclassabc(frozen=True)
        class TransposePolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]

            def __str__(self):
                return f"transpose(\n{self.underlying})"

            def at(self, row: int, col: int) -> PolyDict:
                return self.underlying.at(col, row)


        return state, TransposePolyMatrix(
            underlying=underlying,
            shape=(underlying.shape[1], underlying.shape[0]),
        )
