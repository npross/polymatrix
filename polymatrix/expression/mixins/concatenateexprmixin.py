from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.init import init_block_poly_matrix


class ConcatenateExprMixin(ExpressionBaseMixin):
    """ Concatenate matrices. """

    @property
    @abstractmethod
    def blocks(self) -> tuple[tuple[ExpressionBaseMixin, ...], ...]:
        """ Matrices to concatenate, stored in row major order. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        blocks = {}
        row = 0
        for row_blocks in self.blocks:
            col = 0
            for block_expr in row_blocks:
                state, block = block_expr.apply(state)
                block_nrows, block_ncols = block.shape

                # init_block_polymatrix will check if the ranges are correct
                blocks[range(row, row + block_nrows),
                       range(col, col + block_ncols)] = block

                col += block_ncols
            row += block_nrows

        return state, init_block_poly_matrix(blocks)

