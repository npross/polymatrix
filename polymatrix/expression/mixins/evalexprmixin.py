import abc
import math
import numpy as np

from typing import TypeVar, Callable, Iterable, List, Any

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.index import PolyDict, PolyMatrixDict, VariableIndex
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.symbol import Symbol

# TODO: move this function into the util module (but which one?)
T = TypeVar("T")
def partition(pred: Callable[[T], bool], it: Iterable[T]) -> tuple[List[T], List[T]]:
    """ Partition the given iterable according to the predicate `pred`. """
    ts, fs = [], []
    for item in it:
        if pred(item):
            ts.append(item)
        else:
            fs.append(item)
    return fs, ts


class EvalExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def substitutions(self) -> Iterable[tuple[Symbol, Any]]: ...

    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        p = PolyMatrixDict.empty()

        # Because to each symbol can have multiple indices associated to it
        # for example in a matrix, we need unpack the substitutions into a new
        # map that maps variable indices to single values
        subs: dict[VariableIndex, float] = {}
        try:
            for (sym, values) in self.substitutions:
                # Strict zip to avoid nasty bug where a variable is replaced by a value
                # that has a different shape
                for idx, val in zip(state.get_indices(sym), values, strict=True):
                    subs[idx] = val

        except ValueError as e:
            raise ValueError(f"Cannot replace symbol {sym} that has shape {state.get_shape(sym)} "
                f"with value {values} of shape {np.array(values).shape} because the shapes do not match.") from e

        # Create new polynomial with substituted values
        for entry, poly in underlying.entries():
            new_poly = PolyDict.empty()
            for monomial, coeff in poly.terms():
                not_to_sub, to_sub = partition(lambda var: var.index in subs.keys(), monomial)

                new_monomial = tuple(not_to_sub)
                # TODO: powers of subs could be cached for speed
                new_coeff = coeff * math.prod(subs[v.index] ** v.power for v in to_sub)

                if math.isclose(new_coeff, 0):
                    continue

                if new_monomial in new_poly.keys():
                    new_poly[new_monomial] += new_coeff

                else:
                    new_poly[new_monomial] = new_coeff

            p[entry] = new_poly
        return state, init_poly_matrix(p, underlying.shape)