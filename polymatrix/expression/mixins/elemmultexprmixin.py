import abc
import itertools
import typing
import dataclassabc

from polymatrix.polymatrix.init import init_poly_matrix, init_broadcast_poly_matrix
from polymatrix.polymatrix.index import MonomialIndex
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.utils import merge_monomial_indices


class ElemMultExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def left(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def right(self) -> ExpressionBaseMixin: ...

    @staticmethod
    def elem_mult(
        state: ExpressionState,
        left: PolyMatrix,
        right: PolyMatrix,
    ):
        if left.shape != right.shape:
            if left.shape == (1,1):
                left = init_broadcast_poly_matrix(left.scalar(), shape=right.shape)

            elif right.shape == (1,1):
                right = init_broadcast_poly_matrix(right.scalar(), shape=left.shape)

            else:
                raise NotImplementedError("Cannot do element-wise multiplication of matrices "
                                          f"with shapes {left.shape} and {right.shape}.")

        poly_matrix_data = {}

        for poly_row in range(left.shape[0]):
            for poly_col in range(left.shape[1]):
                polynomial = {}

                left_polynomial = left.get_poly(poly_row, poly_col)
                if left_polynomial is None:
                    continue

                right_polynomial = right.get_poly(poly_row, poly_col)
                if right_polynomial is None:
                    continue

                for (left_monomial, left_value), (
                    right_monomial,
                    right_value,
                ) in itertools.product(
                    left_polynomial.items(), right_polynomial.items()
                ):
                    value = left_value * right_value

                    # if value == 0:
                    #     continue

                    # monomial = tuple(sorted(left_monomial + right_monomial))

                    new_monomial = merge_monomial_indices(
                        (left_monomial, right_monomial)
                    )

                    if new_monomial not in polynomial:
                        polynomial[new_monomial] = 0

                    polynomial[new_monomial] += value

                if 0 < len(polynomial):
                    poly_matrix_data[poly_row, poly_col] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=left.shape,
        )

        return state, poly_matrix

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, left = self.left.apply(state=state)
        state, right = self.right.apply(state=state)

        return self.elem_mult(state, left, right)
