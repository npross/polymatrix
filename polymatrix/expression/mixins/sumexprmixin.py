import abc
import collections
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class SumExprMixin(ExpressionBaseMixin):
    """
    For each row of the matrix sum the colum elements.

    [[1, 2, 3], [4, 5, 6]]  ->  [[6], [15]]
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        poly_matrix_data = collections.defaultdict(dict)

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                underlying_poly = underlying.get_poly(row, col)

                if underlying_poly is None:
                    continue

                polynomial = poly_matrix_data[row, 0]

                for monomial, value in underlying_poly.items():
                    if monomial in polynomial:
                        polynomial[monomial] += value
                    else:
                        polynomial[monomial] = value

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(underlying.shape[0], 1),
        )

        return state, poly_matrix
