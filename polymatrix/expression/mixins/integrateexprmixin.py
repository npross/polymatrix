import abc
import itertools

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.utils.getderivativemonomials import differentiate_polynomial
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)
from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.utils.tooperatorexception import to_operator_exception


class IntegrateExprMixin(ExpressionBaseMixin):
    """
    integrate w.r.t. x on interval (a, b):

    [[c*x**2]]  ->  [[(1/3)*c*(b**3 - a**3)]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def from_(self) -> tuple[float, ...]: ...

    @property
    @abc.abstractmethod
    def to(self) -> tuple[float, ...]: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, variables = get_variable_indices_from_variable(state, self.variables)

        if not (underlying.shape[1] == 1):
            raise AssertionError(
                to_operator_exception(
                    message=f"{underlying.shape[1]=} is not 1",
                    stack=self.stack,
                )
            )

        if not (len(variables) == len(self.from_)):
            raise AssertionError(
                to_operator_exception(
                    message=f"length of tuples do not match: {variables=}, {self.from_=}, {self.to}",
                    stack=self.stack,
                )
            )

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            underlying_poly = underlying.get_poly(row, 0)

            if underlying_poly is None:
                continue

            # integrate each variable and map result to the corresponding column
            for col, (variable, from_, to) in enumerate(
                zip(variables, self.from_, self.to)
            ):
                integrated_polynomial = dict()

                for monomial, value in underlying_poly.items():
                    monomial_cnt = dict(monomial)

                    if variable in monomial_cnt:

                        def acc_integrated_term(acc, v):
                            monomial, value = acc
                            curr_var, curr_var_cnt = v

                            if curr_var is variable:
                                exponent = curr_var_cnt + 1
                                return monomial, value * (
                                    to**exponent - from_**exponent
                                ) / exponent

                            else:
                                return monomial + v, value

                        *_, (integrated_monomial, value) = itertools.accumulate(
                            monomial, acc_integrated_term, initial=(tuple(), 1)
                        )
                        integrated_polynomial[integrated_monomial] = value

                if 0 < len(integrated_polynomial):
                    poly_matrix_data[row, col] = integrated_polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(underlying.shape[0], len(variables)),
        )

        return state, poly_matrix
