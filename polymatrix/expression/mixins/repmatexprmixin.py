import abc
import dataclassabc

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.index import PolyDict


class RepMatExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractclassmethod
    def repetition(self) -> tuple[int, int]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        # FIXME: move to polymatrix module
        @dataclassabc.dataclassabc(frozen=True)
        class RepMatPolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]

            def at(self, row: int, col: int) -> PolyDict:
                n_row, n_col = underlying.shape

                rel_row = row % n_row
                rel_col = col % n_col

                return self.underlying.at(rel_row, rel_col)

        return state, RepMatPolyMatrix(
            underlying=underlying,
            shape=tuple(s * r for s, r in zip(underlying.shape, self.repetition)),
        )
