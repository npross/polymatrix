import abc

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.utils import multiply_polynomial
from polymatrix.utils.tooperatorexception import to_operator_exception


class MatrixMultExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def left(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def right(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, left = self.left.apply(state=state)
        state, right = self.right.apply(state=state)

        if not (left.shape[1] == right.shape[0]):
            msg = f"Cannot multiply matrices {self.left} and {self.right} because their shapes " \
                    f"{left.shape}, and {right.shape} do not match!"
            raise AssertionError(
                to_operator_exception(
                    message=msg,
                    stack=self.stack,
                )
            )

        poly_matrix_data = {}

        for poly_row in range(left.shape[0]):
            for poly_col in range(right.shape[1]):
                polynomial = {}

                for index_k in range(left.shape[1]):
                    left_polynomial = left.get_poly(poly_row, index_k)
                    if left_polynomial is None:
                        continue

                    right_polynomial = right.get_poly(index_k, poly_col)
                    if right_polynomial is None:
                        continue

                    multiply_polynomial(left_polynomial, right_polynomial, polynomial)

                if 0 < len(polynomial):
                    poly_matrix_data[poly_row, poly_col] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(left.shape[0], right.shape[1]),
        )

        return state, poly_matrix
