import abc

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class LegendreSeriesMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> tuple[ExpressionBaseMixin]: ...

    @property
    @abc.abstractmethod
    def degrees(self) -> tuple[int, ...] | None: ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state)

        if self.degrees is None:
            degrees = range(underlying.shape[0])
        else:
            degrees = self.degrees

        poly_matrix_data = {}

        for degree in degrees:
            # for degree in self.degree:
            poly = underlying.get_poly(degree, 0)

            poly_matrix_data[degree, 0] = dict(poly)

            if 2 <= degree:
                poly = underlying.get_poly(degree - 2, 0)
                factor = -(degree - 1) / (degree + 1)

                for m, v in poly.items():
                    if m in poly_matrix_data[degree, 0]:
                        poly_matrix_data[degree, 0][m] += v * factor
                    else:
                        poly_matrix_data[degree, 0][m] = v * factor

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(len(poly_matrix_data), 1),
        )

        return state, poly_matrix
