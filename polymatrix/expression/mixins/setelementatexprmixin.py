import abc
import dataclasses
import typing
import dataclassabc
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.index import PolyDict
from polymatrix.expressionstate import ExpressionState


class SetElementAtExprMixin(ExpressionBaseMixin):
    @dataclasses.dataclass(frozen=True)
    class Slice:
        start: int
        step: int
        stop: int

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def index(self) -> tuple[tuple[int, ...], tuple[int, ...]]: ...

    @property
    @abc.abstractmethod
    def value(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, polynomial_expr = self.value.apply(state=state)

        assert polynomial_expr.shape == (1, 1)

        polynomial = polynomial_expr.get_poly(0, 0)
        if polynomial is None:
            polynomial = 0

        # FIXME: move to polymatrix module
        @dataclassabc.dataclassabc(frozen=True)
        class SetElementAtPolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin
            shape: tuple[int, int]
            index: tuple[int, int]
            polynomial: PolyDict

            def at(self, row: int, col: int) -> PolyDict:
                if (row, col) == self.index:
                    return self.polynomial
                else:
                    return self.underlying.at(row, col)

        return state, SetElementAtPolyMatrix(
            underlying=underlying,
            index=self.index,
            shape=underlying.shape,
            polynomial=polynomial,
        )
