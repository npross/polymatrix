import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


class TruncateExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> ExpressionBaseMixin | None: ...

    @property
    @abc.abstractmethod
    def degrees(self) -> tuple[int]: ...

    @property
    @abc.abstractmethod
    def inverse(self) -> bool: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        if self.variables is None:
            cond = lambda idx: True

        else:
            state, variable_indices = get_variable_indices_from_variable(
                state, self.variables
            )
            cond = lambda idx: idx in variable_indices

        poly_matrix_data = {}

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                polynomial = underlying.get_poly(row, col)
                if polynomial is None:
                    continue

                polynomial = {}

                for monomial, value in polynomial.items():
                    degree = sum(
                        (count for var_idx, count in monomial if cond(var_idx))
                    )

                    if (degree in self.degrees) is not self.inverse:
                        polynomial[monomial] = value

                poly_matrix_data[row, col] = polynomial

        poly_matrix = init_poly_matrix(
            data=dict(poly_matrix_data),
            shape=underlying.shape,
        )

        return state, poly_matrix
