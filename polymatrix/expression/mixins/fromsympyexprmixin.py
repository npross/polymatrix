import sympy
import math

from abc import abstractmethod
from typing_extensions import override, cast

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState

from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.init import init_poly_matrix 
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MonomialIndex, VariableIndex
from polymatrix.symbol import Symbol


class FromSympyExprMixin(ExpressionBaseMixin):
    """
    Make an expression from a sympy expression. The sympy expression can be
    scalar or matrix (`sympy.Matrix`).

    ..code:: py
        x, y = sympy.symbols('x, y')

        # scalar bivariate polynomial
        p_sym = x**2 + y + 2
        p = polymatrix.from_sympy(p_sym)

        # 2D vector field
        q_sym = sympy.Matrix(((x + y), (x**2 + 1)))
        q = polymatrix.from_sympy(q_sym)
    """

    @property
    @abstractmethod
    def data(self) -> sympy.Expr | tuple[tuple[sympy.Expr]] | sympy.Matrix:
        """ The sympy objects. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:

        # Unpack if it is a sympy matrix
        if isinstance(self.data, sympy.Matrix):
            data = cast(tuple[tuple[sympy.Expr]],
                        ((entry for entry in self.data.row(r))
                         for r in range(self.data.rows)))

        # Pack if is raw (scalar) expression
        elif isinstance(self.data, sympy.Expr):
            data = ((self.data,),)

        else:
            data = self.data

        # Convert to polymatrix
        polymatrix = PolyMatrixDict.empty()
        for r, row in enumerate(data):
            for c, entry in enumerate(row):
                # Special case of constant polynomial
                if isinstance(entry, int | float):
                    if not math.isclose(entry, 0):
                        polymatrix[r, c] = PolyDict({
                            MonomialIndex.constant(): entry,
                        })
                    continue

                try:
                    sympy_poly = sympy.poly(entry)

                except sympy.polys.polyerrors.BasePolynomialError as e:
                    raise ValueError(f"Cannot convert sympy expression {entry} "
                    "into a polynomial, are you sure it is a polynomial?") from e

                # Convert sympy variables to our variables
                sympy_to_var = {
                    sympy_idx: Symbol(var.name)
                    for sympy_idx, var in enumerate(sympy_poly.gens)
                }

                # Construct poynomial
                poly = PolyDict.empty()
                for coeff, monom in zip(sympy_poly.coeffs(), sympy_poly.monoms()):
                    # sympy monomial is stored as multi-index, eg. for a
                    # multivariate polynomial with three variables (x, y, z)
                    # the index is x * y**2 = (1, 2, 0)
                    m: list[VariableIndex] = []
                    for i, exponent in enumerate(monom):
                        var = sympy_to_var[i]
                        state, idx = state.index(var, shape=(1,1))
                        # idx.start because var is a scalar
                        m.append(VariableIndex(idx.start, exponent))

                    poly[m] = coeff
                polymatrix[r, c] = poly

        shape = (r + 1, c + 1)
        return state, init_poly_matrix(polymatrix, shape)


