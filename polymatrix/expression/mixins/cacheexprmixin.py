import abc
import dataclasses

from polymatrix.polymatrix.mixins import PolyMatrixAsDictMixin
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class CacheExprMixin(ExpressionBaseMixin):
    """Caches the polynomial matrix using the state"""

    @property
    @abc.abstractclassmethod # FIXME: not a classmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrixMixin]: # FIXME: return type
        if self in state.cache:
            return state, state.cache[self]

        state, underlying = self.underlying.apply(state)

        if isinstance(underlying, PolyMatrixAsDictMixin):
            cached_data = underlying.data
        else:
            cached_data = dict(underlying.gen_data())

        poly_matrix = init_poly_matrix(
            data=cached_data,
            shape=underlying.shape,
        )

        state = dataclasses.replace(
            state,
            cache=state.cache | {self: poly_matrix},
        )

        return state, poly_matrix
