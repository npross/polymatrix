import abc
import dataclassabc

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.index import PolyDict


class DiagExprMixin(ExpressionBaseMixin):
    """
    [[1],[2]]  ->  [[1,0],[0,2]]

    or

    [[1,0],[0,2]]  ->  [[1],[2]]
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    # FIXME: typing
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)

        # Vector to diagonal matrix
        if underlying.shape[1] == 1:

            # FIXME: move to polymatrix module
            @dataclassabc.dataclassabc(frozen=True)
            class DiagFromVecPolyMatrix(PolyMatrixMixin):
                underlying: PolyMatrixMixin
                shape: tuple[int, int]

                def __str__(self):
                    return f"diag(\n{self.underlying})"

                def at(self, row: int, col: int) -> PolyDict:
                    if row != col:
                        return PolyDict.empty()

                    return self.underlying.get_poly(row, 0)

            return state, DiagFromVecPolyMatrix(
                underlying=underlying,
                shape=(underlying.shape[0], underlying.shape[0]),
            )

        # Diagonal matrix to vector
        else:
            # FIXME: replace assertions with meaningful error message
            assert underlying.shape[0] == underlying.shape[1], f"{underlying.shape=}"

            # FIXME: move to polymatrix module
            @dataclassabc.dataclassabc(frozen=True)
            class VecFromDiagPolyMatrix(PolyMatrixMixin):
                underlying: PolyMatrixMixin
                shape: tuple[int, int]

                def __str__(self):
                    return f"diag(\n{self.underlying})"

                def at(self, row: int, _col: int) -> PolyDict:
                    return self.underlying.at(row, row)

            return state, VecFromDiagPolyMatrix(
                underlying=underlying,
                shape=(underlying.shape[0], 1),
            )
