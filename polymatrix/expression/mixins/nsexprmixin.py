from __future__ import annotations

from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyDict, MonomialIndex
from polymatrix.polymatrix.init import init_broadcast_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin

class NsExprMixin(ExpressionBaseMixin):
    """
    Make a matrix or vector that is filled with N's. This is used to provide
    functions line ones() or zeros(), i.e. N = 1 and N = 0 respectively. N may
    not only be a number but also any scalar expression.
    """

    @property
    @abstractmethod
    def n(self) -> int | float | ExpressionBaseMixin:
        """ Value that is used to fill the matrix """

    @property
    @abstractmethod
    def shape(self) -> tuple[int, int] | ExpressionBaseMixin:
        """
        Shape of the ones. If it is an expression it must evaluate to a 2d
        column vector of integers.
        """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        if isinstance(self.n, ExpressionBaseMixin):
            state, pm = self.n.apply(state)
            n = pm.scalar()

        else:
            n = PolyDict({ MonomialIndex.constant(): 1. })

        if isinstance(self.shape, ExpressionBaseMixin):
            state, pm = self.shape.apply(state)
            if pm.shape != (2, 1):
                raise ValueError("Shape must evaluate to a 2d column vector "
                                 f"but it has shape {pm.shape}")

            # FIXME: should check that they are actually integers
            nrows = int(pm.at(0, 0).constant())
            ncols = int(pm.at(1, 0).constant())

        elif isinstance(self.shape, tuple):
            if isinstance(self.shape[0], ExpressionBaseMixin):
                state, nrows_pm = self.shape[0].apply(state)
                nrows = int(nrows_pm.scalar().constant())

            elif isinstance(self.shape[0], int):
                nrows = self.shape[0]

            else:
                # TODO: error message
                raise TypeError

            if isinstance(self.shape[1], ExpressionBaseMixin):
                state, ncols_pm = self.shape[1].apply(state)
                ncols = int(ncols_pm.scalar().constant())

            elif isinstance(self.shape[1], int):
                ncols = self.shape[1]

            else:
                # TODO: error message
                raise TypeError

        return state, init_broadcast_poly_matrix(n, shape=(nrows, ncols))




