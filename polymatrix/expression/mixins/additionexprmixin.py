from __future__ import annotations

import math

from abc import abstractmethod
from typing_extensions import override

from polymatrix.utils.getstacklines import FrameSummary
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.index import PolyMatrixDict
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.utils.broadcastpolymatrix import broadcast_poly_matrix


class AdditionExprMixin(ExpressionBaseMixin):
    """
    Adds two polymatrices

        [[2*x1+x2], [x1**2]] + [[3*x2], [x1]]  ->  [[2*x1+4*x2], [x1+x1**2]].

    If one summand is of size (1, 1), then perform broadcast:

        [[2*x1+x2], [x1**2]] + [[x1]]  ->  [[3*x1+x2], [x1+x1**2]].
    """

    @property
    @abstractmethod
    def left(self) -> ExpressionBaseMixin: ...

    @property
    @abstractmethod
    def right(self) -> ExpressionBaseMixin: ...

    @property
    @abstractmethod
    def stack(self) -> tuple[FrameSummary]: ...

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, left = self.left.apply(state=state)
        state, right = self.right.apply(state=state)

        left, right = broadcast_poly_matrix(left, right, self.stack)

        # Keep left as-is and add the stuff from right
        result = PolyMatrixDict.empty()
        for entry, right_poly in right.entries():
            new_poly = left.at(*entry)
            for monomial, coeff in right_poly.terms():
                if monomial in new_poly:
                    new_poly[monomial] += coeff

                    if math.isclose(new_poly[monomial], 0):
                        del new_poly[monomial]
                else:
                    new_poly[monomial] = coeff

            result[entry] = new_poly
        return state, init_poly_matrix(result, left.shape)
