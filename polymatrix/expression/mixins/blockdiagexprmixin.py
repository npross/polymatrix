from abc import abstractmethod
from typing_extensions import override

from polymatrix.polymatrix.mixins import BlockPolyMatrixMixin
from polymatrix.polymatrix.init import init_block_poly_matrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class BlockDiagExprMixin(ExpressionBaseMixin):
    """
    Create a block diagonal matrix from a tuple of matrices
    ::

        (A, B, C) -> [ A     ]
                     [   B   ]
                     [     C ]
    """

    @property
    @abstractmethod
    def blocks(self) -> tuple[ExpressionBaseMixin, ...]: ...

    @override
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, BlockPolyMatrixMixin]:

        d, row, col = {}, 0, 0
        for block in self.blocks:
            state, pm = block.apply(state)
            block_nrows, block_ncols = pm.shape

            d[range(row, row + block_nrows),
              range(col, col + block_ncols)] = pm

            row += block_nrows
            col += block_ncols

        return state, init_block_poly_matrix(d)
