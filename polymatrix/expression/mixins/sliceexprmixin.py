from __future__ import annotations

from abc import abstractmethod
from typing import NamedTuple
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.polymatrix.init import init_slice_poly_matrix


# For some reason in older python versions slice is not a hashable
# type and this causes crashes when old code uses the cache in the state object
# cf. https://stackoverflow.com/questions/29980786/why-are-slice-objects-not-hashable-in-python
# FIXME: Move this class to the correct place, eg. in global typing module or
# find a solution
class HashableSlice(NamedTuple):
    start: int | None
    stop: int | None
    step: int | None


class SliceExprMixin(ExpressionBaseMixin):
    """
    Take a slice (one or more elements) of a matrix.

    Examples:
    Suppose there is the following matrix

    M = 
    [ a b c d ]
    [ e f g h ]
    [ i j k l ]
    [ m n o p ]

    By slicing M[0,3] we get element i.
    By slicing M[:,3] we get the column [[c] [g] [k] [o]]
    By slicing M[:2,:2] we get the submatrix

        [ a b ]
        [ e f ]

    and so on.
    """

    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        """ Expression to take the slice from. """

    @property
    @abstractmethod
    def slice(self) -> tuple:
        """ The slice. """
        # Type / format of this property must match of slice accepted by
        # SlicePolyMatrix, since it directly uses that see
        # polymatrix.polymatrix.init.init_poly_matrix

        # TODO: allow slice to be an Expression that evaluates to a number or
        # vector of numbers

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, p = self.underlying.apply(state)
        return state, init_slice_poly_matrix(p, self.slice)

