import abc

from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class ExpressionBaseMixin(abc.ABC):
    @abc.abstractmethod
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]: ...
