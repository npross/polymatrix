import abc
import collections
import dataclassabc

from polymatrix.polymatrix.index import PolyDict
from polymatrix.polymatrix.mixins import PolyMatrixMixin

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState


class SymmetricExprMixin(ExpressionBaseMixin):
    """
    Maps a square matrix to a symmetric square matrix by taking the average of the diagonal elements.

    [[1, 2], [3, 4]]  ->  [[1, 2.5], [2.5, 4]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        assert underlying.shape[0] == underlying.shape[1]

        # FIXME: move to polymatrix module
        @dataclassabc.dataclassabc(frozen=True)
        class SymmetricPolyMatrix(PolyMatrixMixin):
            underlying: PolyMatrixMixin

            def __str__(self):
                return f"symmetric(\n{self.underlying})"

            @property
            def shape(self) -> tuple[int, int]:
                return self.underlying.shape

            def at(self, row: int, col: int) -> PolyDict:
                # FIXME: this is a quick workaround
                return PolyDict(self.get_poly(row, col) or {})

            def get_poly(self, row: int, col: int) -> dict[tuple[int, ...], float]:
                def gen_symmetric_monomials():
                    for i_row, i_col in ((row, col), (col, row)):
                        polynomial = self.underlying.get_poly(i_row, i_col)

                        if polynomial is not None:
                            yield polynomial

                all_monomials = tuple(gen_symmetric_monomials())

                if len(all_monomials) == 0:
                    return None

                else:
                    polynomial = collections.defaultdict(float)

                    # merge monomials
                    for monomials in all_monomials:
                        for monomial, value in monomials.items():
                            polynomial[monomial] += value / 2

                    return dict(polynomial)
                    # return terms

        polymatrix = SymmetricPolyMatrix(
            underlying=underlying,
        )

        return state, polymatrix
