
import abc

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import MatrixIndex, PolyMatrixDict, PolyDict, MonomialIndex
from polymatrix.utils.getstacklines import FrameSummary


class DegreeExprMixin(ExpressionBaseMixin):
    """
    Elementwise maximum degree.

    The result is a constant matrix with each entry containing the highest
    degree of the polynomial in the entry of the argument.
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        ...

    @property
    @abc.abstractmethod
    def stack(self) -> tuple[FrameSummary]:
        ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:

        state, p = self.underlying.apply(state=state)
        result = PolyMatrixDict.empty()

        for entry, poly in p.entries():
            max_degree = 0
            for monomial, coeff in poly.terms():
                if monomial.degree > max_degree:
                    max_degree = monomial.degree

            result[entry] = PolyDict({
                MonomialIndex.constant(): max_degree
            })

        return state, init_poly_matrix(data=result, shape=p.shape)
