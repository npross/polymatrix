import abc
import collections

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState


class ToConstantExprMixin(ExpressionBaseMixin):
    """
    Discards non-constant polynomial coefficients

    [[a + b*x + c*x^2]]  ->  [[a]]
    """

    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)

        poly_matrix_data = collections.defaultdict(dict)

        for row in range(underlying.shape[0]):
            for col in range(underlying.shape[1]):
                polynomial = underlying.get_poly(row, col)
                if polynomial is None:
                    continue

                if tuple() in polynomial:
                    poly_matrix_data[row, col][tuple()] = polynomial[tuple()]

        poly_matrix = init_poly_matrix(
            data=dict(poly_matrix_data),
            shape=underlying.shape,
        )

        return state, poly_matrix
