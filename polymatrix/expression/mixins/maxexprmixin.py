from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyMatrixDict, MatrixIndex, PolyDict, MonomialIndex
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class MaxExprMixin(ExpressionBaseMixin):
    """
    Keep only the maximum of an element of vector, returns a scalar. If the
    underlying is a matrix then the maximum is interpreted row-wise, so it
    returns a column vector.

    The vector (or matrix) may not contain polynomials.
    """

    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state)
        nrows, ncols = underlying.shape

        # row vector
        if nrows == 1:
            maximum = None
            for col in range(ncols):
                if poly := underlying.at(0, col):
                    if poly.degree > 0:
                        raise ValueError("Cannot take maximum of non-costant vector. "
                                         f"Entry at {(0,col)} has degree {poly.degree}.")

                    if maximum is None or poly.constant() > maximum:
                        maximum = poly.constant()

            assert maximum is not None

            shape = (1, 1)
            p = PolyMatrixDict({
                MatrixIndex(0, 0): PolyDict({
                    MonomialIndex.constant(): maximum
                })
            })

        # column vector
        elif ncols == 1:
            maximum = None
            for row in range(nrows):
                if poly := underlying.at(row, 0):
                    if poly.degree > 0:
                        raise ValueError("Cannot take maximum of non-costant vector. "
                                         f"Entry at {(row, 0)} has degree {poly.degree}.")

                    if maximum is None or poly.constant() > maximum:
                        maximum = poly.constant()

            assert maximum is not None
            
            shape = (1, 1)
            p = PolyMatrixDict({
                MatrixIndex(0, 0): PolyDict({
                    MonomialIndex.constant(): maximum
                })
            })

        # matrix
        else:
            maxima = []
            for row in range(nrows):
                maximum = None
                for col in range(ncols):
                    if poly := underlying.at(row, col):
                        if poly.degree > 0:
                            raise ValueError("Cannot take maximum of non-costant vector. "
                                             f"Entry at {(row, 0)} has degree {poly.degree}.")

                        if maximum is None or poly.constant() > maximum:
                            maximum = poly.constant()

                assert maximum is not None
                maxima.append(maximum)

            shape = (nrows, 1)
            p = PolyMatrixDict({
                MatrixIndex(i, 0): PolyDict({
                    MonomialIndex.constant(): row_max
                })
                for i, row_max in enumerate(maxima)
            })

        return state, init_poly_matrix(p, shape)
