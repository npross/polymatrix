from __future__ import annotations

import math

from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expression.mixins.elemmultexprmixin import ElemMultExprMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin


class PowerExprMixin(ExpressionBaseMixin):
    """
    Raise an expression to an integral power. If the expression is not scalar,
    it is interpreted as elementwise exponentiation.

    The exponent may be an expression, but the expression must evaluate to an
    integer constant.
    """

    @property
    @abstractmethod
    def left(self) -> ExpressionBaseMixin: ...

    @property
    @abstractmethod
    def right(self) -> ExpressionBaseMixin | int | float: ...

    @staticmethod
    def power(
        state: ExpressionState,
        left: ExpressionBaseMixin,
        right: ExpressionBaseMixin | int | float
    ) -> tuple[ExpressionState, PolyMatrixMixin]:
        """ Compute the expression ```left ** right```. """
        exponent: int | None = None

        # Right (exponent) must end up being a scalar constant
        if isinstance(right, int):
            exponent = right
            
        elif isinstance(right, float):
            exponent = int(right)
            if not math.isclose(right - exponent, 0):
                raise ValueError("Cannot raise a variable to a non-integral power. "
                                 f"Exponent {right} (float) is not close enough to an integer.")

        elif isinstance(right, ExpressionBaseMixin):
            state, right_polymatrix = right.apply(state)

            r = right_polymatrix.at(0, 0).constant()
            if not isinstance(r, int):
                exponent = int(r)
                if not math.isclose(r - exponent, 0):
                    raise ValueError("Cannot raise a variable to a non-integral power. "
                                     f"Exponent {right}, resulting from {right} is not an integer.")
            else:
                exponent = r

        else:
            raise TypeError(f"Cannot raise {left} to {right}, ",
                            f"because exponet has type {type(right)}")

        state, base = left.apply(state)
        result = base
        for _ in range(exponent -1):
            state, result = ElemMultExprMixin.elem_mult(state, result, base)

        return state, result

    @override
    def apply(self, state: ExpressionStateMixin) -> tuple[ExpressionStateMixin, PolyMatrixMixin]:
        # Separated into staticmethod like elem_mult for code resuse in
        # CombinationsExprMixin
        return self.power(state, self.left, self.right)

