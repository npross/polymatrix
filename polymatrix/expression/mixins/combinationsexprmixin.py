import abc
import itertools
import math
from typing import Iterable

from polymatrix.polymatrix.utils import multiply_polynomial
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class CombinationsExprMixin(ExpressionBaseMixin):
    # FIXME: improve docstring
    """
    combination using degrees=(0, 1, 2, 3):

    [[x]]  ->  [[1], [x], [x**2], [x**3]]
    """

    @property
    @abc.abstractmethod
    def expression(self) -> ExpressionBaseMixin:
        """ Column vector. """

    @property
    @abc.abstractmethod
    def degrees(self) -> ExpressionBaseMixin | tuple[int, ...]:
        """
        Vector or scalar expression, or a list of integers.
        """

    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, expr_pm = self.expression.apply(state)
        
        degrees: Iterable | None = None

        if isinstance(self.degrees, ExpressionBaseMixin):
            state, deg_pm = self.degrees.apply(state)

            # Check that it is a constant
            for entry, poly in deg_pm.entries():
                if not poly.is_constant():
                    # FIXME: improve error message
                    raise ValueError("Non-constant exponent resulting from "
                        f"evaluating {self.degrees}. Exponent must be a constant!")
            
            # Scalars are OK
            nrows, ncols = deg_pm.shape
            if nrows == 1 and ncols == 1:
                degrees = (deg_pm.scalar().constant(),)

            # Column vectors are OK
            elif ncols == 1:
                degrees = tuple(deg_pm.at(i, 0).constant()
                                for i in range(nrows))

            # Row vectors are OK
            elif nrows == 1:
                degrees = tuple(deg_pm.at(0, i).constant()
                                for i in range(ncols))

            # Matrices are not OK
            else:
                raise ValueError(f"Invalid exponent with shape {deg_pm.shape} resulting from {self.degrees} "
                                 "Exponent can only be a scalar or a vector of exponents "
                                 "(multi-index), matrices are not allowed.")

        elif isinstance(self.degrees, tuple):
            degrees = self.degrees

        # check that degrees are all integers
        for deg in degrees:
            if not math.isclose(int(deg), deg):
                raise ValueError(f"Non-integer degrees ({deg}) are not supported.")

        degrees = tuple(int(d) for d in degrees)

        # FIXME: improve error message
        assert expr_pm.shape[1] == 1

        def gen_indices():
            for degree in degrees:
                yield from itertools.combinations_with_replacement(
                    range(expr_pm.shape[0]), degree
                )

        indices = tuple(gen_indices())

        poly_matrix_data = {}

        for row, indexing in enumerate(indices):
            # x.combinations((0, 1, 2)) produces [1, x, x**2]
            if len(indexing) == 0:
                poly_matrix_data[row, 0] = {tuple(): 1.0}
                continue

            def acc_product(left, row):
                right = expr_pm.get_poly(row, 0)

                if len(left) == 0:
                    return right

                result = {}
                multiply_polynomial(left, right, result)
                return result

            *_, polynomial = itertools.accumulate(
                indexing,
                acc_product,
                initial={},
            )

            poly_matrix_data[row, 0] = polynomial

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(len(poly_matrix_data), 1),
        )

        return state, poly_matrix
