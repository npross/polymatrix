from __future__ import annotations

from abc import abstractmethod
from math import sqrt, isclose
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyMatrixDict
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin

class LowerTriangularExprMixin(ExpressionBaseMixin):
    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        r"""
        Construct a lower triangular matrix from a vector.

        For a :math:`n\times n` lower diagonal matrix, this is a :math:`n * (n
        + 1) / 2` dimensional vector containing the entries. The lower diagonal
        matrix is filled from top to bottom so given the vector :math:`z` the 
        lower triangular matrix is

        .. math::
            Z = \begin{bmatrix}
                z_1 \\
                z_2 & z_3 \\
                z_4 & z_5 & z_6 \\
                \vdots &  & & \ddots \\
            \end{bmatrix}.

        """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, u = self.underlying.apply(state)

        N, ncols = u.shape
        if ncols > 1:
            raise ValueError("Cannot construct lower triangular matrix from object "
                             f"with shape {u.shape}, it must be a column vector.")

        n_float = .5 * (-1 + sqrt(1 + 8 * N))
        if not isclose(int(n_float), n_float):
            raise ValueError("To construct a n x n lower triangular matrix, the vector "
                             "must be an n * (n + 1) / 2 dimensional column.")
        
        n = int(n_float)
        p = PolyMatrixDict.empty()

        for row in range(n):
            i = row * (row + 1) // 2
            for col in range(row + 1):
                p[row, col] = u.at(i + col, 0)

        return state, init_poly_matrix(p, shape=(n, n))



