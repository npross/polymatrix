import abc
import collections
import typing

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.utils.getderivativemonomials import differentiate_polynomial
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


class DivergenceExprMixin(ExpressionBaseMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> typing.Union[tuple, ExpressionBaseMixin]: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrix]:
        state, underlying = self.underlying.apply(state=state)
        state, variables = get_variable_indices_from_variable(state, self.variables)

        # NP: replace with exception with error explaination
        assert underlying.shape[1] == 1, f"{underlying.shape=}"
        assert (
            len(variables) == underlying.shape[0]
        ), f"{variables=}, {underlying.shape=}"

        polynomial_data = collections.defaultdict(float)

        for row, variable in enumerate(variables):
            # NP: did you know that python has the walrus operator? `:=`
            # NP: if poly := underlying.get_poly(row, 0):
            polynomial = underlying.get_poly(row, 0)
            if polynomial is None:
                continue

            state, derivation = differentiate_polynomial(
                polynomial=polynomial,
                diff_wrt_variable=variable,
                state=state,
                considered_variables=set(),
                introduce_derivatives=False,
            )

            for monomial, value in derivation.items():
                polynomial_data[monomial] += value

        poly_matrix = init_poly_matrix(
            data={(0, 0): dict(polynomial_data)},
            shape=(1, 1),
        )

        return state, poly_matrix
