from __future__ import annotations

from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MonomialIndex
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin

class ARangeExprMixin(ExpressionBaseMixin):
    """
    Create a column vector of evenly spaced integer values in an interval.
    Essentially a wrapper around python's `range` built-in function.
    """

    @property
    @abstractmethod
    def start(self) -> int | ExpressionBaseMixin | None:
        """ Start of the range """

    @property
    @abstractmethod
    def stop(self) -> int | ExpressionBaseMixin:
        """ End of the range, this value is not included in the interval. """

    @property
    @abstractmethod
    def step(self) -> int | ExpressionBaseMixin | None:
        """ Step of the range """


    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        if not self.start:
            start = 0

        elif isinstance(self.start, int):
            start = self.start
        elif isinstance(self.start, ExpressionBaseMixin):
            state, pm = self.start.apply(state)
            # TODO: check that is actually an integer
            start = int(pm.scalar().constant())
        else:
            raise TypeError

        if isinstance(self.stop, int):
            stop = self.stop
        elif isinstance(self.stop, ExpressionBaseMixin):
            state, pm = self.stop.apply(state)
            # TODO: check that is actually an integer
            stop = int(pm.scalar().constant())
        else:
            raise TypeError

        if not self.step:
            step = 1
        elif isinstance(self.step, int):
            step = self.step
        elif isinstance(self.step, ExpressionBaseMixin):
            state, pm = self.step.apply(state)
            # TODO: check that is actually an integer
            step = int(pm.scalar().constant())
        else:
            raise TypeError

        p = PolyMatrixDict.empty()
        values = tuple(range(start, stop, step))

        for r, v in enumerate(values):
            p[r, 0] = PolyDict({ MonomialIndex.constant(): v })

        return state, init_poly_matrix(p, shape=(len(values), 1))




