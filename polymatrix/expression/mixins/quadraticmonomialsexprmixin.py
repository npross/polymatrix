import abc
import dataclasses

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)
from polymatrix.polymatrix.utils import split_monomial_indices


class QuadraticMonomialsExprMixin(ExpressionBaseMixin):
    # FIXME: docstring, what does this thing even do
    """
    Maps a polynomial matrix

        underlying = [
            [x y    ],
            [x + x^2],
        ]

    into a vector of monomials

        output = [1, x, y]

    in variable

        variables = [x, y].
    """

    @property
    @abc.abstractclassmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abc.abstractmethod
    def variables(self) -> ExpressionBaseMixin: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(
        self,
        state: ExpressionState,
    ) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, underlying = self.underlying.apply(state=state)
        state, variable_indices = get_variable_indices_from_variable(
            state, self.variables
        )

        def gen_sos_monomials():
            for row in range(underlying.shape[0]):
                for col in range(underlying.shape[1]):
                    polynomial = underlying.get_poly(row, col)
                    if polynomial is None:
                        continue

                    for monomial in polynomial.keys():
                        x_monomial = tuple(
                            (var_idx, count)
                            for var_idx, count in monomial
                            if var_idx in variable_indices
                        )

                        left, right = split_monomial_indices(x_monomial)

                        yield left
                        yield right

        sos_monomials = tuple(
            sorted(set(gen_sos_monomials()), key=lambda m: (len(m), m))
        )

        def gen_data():
            for index, monomial in enumerate(sos_monomials):
                yield (index, 0), {monomial: 1.0}

        poly_matrix_data = dict(gen_data())

        poly_matrix = init_poly_matrix(
            data=poly_matrix_data,
            shape=(len(sos_monomials), 1),
        )

        state = dataclasses.replace(
            state,
            cache=state.cache | {self: poly_matrix},
        )

        return state, poly_matrix
