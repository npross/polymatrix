from __future__ import annotations

import math

from abc import abstractmethod
from typing_extensions import override

from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin


class NegationExprMixin(ExpressionBaseMixin):
    """ Negate expression, or, multiply by -1. """

    @property
    @abstractmethod
    def underlying(self):
        """ The expression that will be negated. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, u = self.underlying.apply(state)
        p = PolyMatrixDict.empty()
        for entry, poly in u.entries():
            p[entry] = PolyDict.empty()
            for monomial, coeff in poly.terms():
                p[entry][monomial] = -coeff

        return state, init_poly_matrix(p, u.shape)

