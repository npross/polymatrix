from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.expression import Expression
from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.statemonad import StateMonad

class FromStateMonadMixin(ExpressionBaseMixin):
    """
    Make a expression from a `StateMonad` object wrapping a function
    that returns one of the following types:

      - Expression
      - ExpressionBaseMixin
      - PolyMatrix
    """

    @property
    @abstractmethod
    def monad(self) -> StateMonad:
        """ The state monad object. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        state, expr = self.monad.apply(state)
        # Case when monad wraps functions 
        #  f: ExpressionState -> (ExpressionState, Expression)
        #  f: ExpressionState -> (ExpressionState, Mixin)
        if isinstance(expr, Expression | ExpressionBaseMixin):
            return expr.apply(state)

        # Case when monad wraps function 
        #  f: ExpressionState -> (ExpressionState, PolyMatrix)
        elif isinstance(expr, PolyMatrixMixin):
            return state, expr

        else:
            raise TypeError(f"Return type of StateMonad object {self.monad} "
                            "must be of type Expression or PolyMatrix!")
