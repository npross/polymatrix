from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.abc import PolyMatrix
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.index import PolyMatrixDict, PolyDict, MatrixIndex, MonomialIndex

class ShapeExprMixin(ExpressionBaseMixin):
    """
    Get the shape of a polymatrix.
    This gives the shape as a row vector [[nrows], [ncols]].
    """
    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        """ The expression for which we compute the shape. """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrix]:
        state, u = self.underlying.apply(state)
        nrows, ncols = u.shape
        p = PolyMatrixDict({
            MatrixIndex(0, 0): PolyDict({MonomialIndex.constant(): nrows}),
            MatrixIndex(1, 0): PolyDict({MonomialIndex.constant(): ncols})
        })

        return state, init_poly_matrix(p, shape=(2,1))

