from __future__ import annotations

from abc import abstractmethod
from typing_extensions import override

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyDict, MonomialIndex
from polymatrix.polymatrix.init import init_broadcast_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin

class NamedExprMixin(ExpressionBaseMixin):
    """
    Give a name to an expression.

    This is mostly to make the printing more understandable.
    See also NamedExprImpl.__str__.
    """

    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin:
        """ The expresssion """

    @property
    @abstractmethod
    def name(self) -> str:
        """ The name for the expression """

    @override
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:
        return self.underlying.apply(state)
