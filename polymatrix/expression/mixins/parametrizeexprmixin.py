from __future__ import annotations

from abc import abstractmethod
from itertools import product

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.expressionstate import ExpressionState
from polymatrix.polymatrix.index import PolyMatrixDict, MatrixIndex, PolyDict, MonomialIndex, VariableIndex
from polymatrix.polymatrix.init import init_poly_matrix
from polymatrix.polymatrix.mixins import PolyMatrixMixin
from polymatrix.symbol import Symbol 


class ParametrizeExprMixin(ExpressionBaseMixin):
    r"""
    Given matrix or vector (or an expression) :math:`x` and a name, for
    instance :math:`u`, create a new variable :math:`u` of the same shape as
    :math:`x`.

    This is useful if you want to create coefficients, for example:

    ::
        x = polymatrix.v_stack((x_0, x_1, x_2)) # in R^3
        u = x.parametrize('u')

        # Create u_0 * x_0 + u_1 * x_1 + u_2 * x_2
        u.T @ x
    """

    @property
    @abstractmethod
    def underlying(self) -> ExpressionBaseMixin: ...

    @property
    @abstractmethod
    def name(self) -> str: ...

    # overwrites the abstract method of `ExpressionBaseMixin`
    def apply(self, state: ExpressionState) -> tuple[ExpressionState, PolyMatrixMixin]:

        state, underlying = self.underlying.apply(state)
        nrows, ncols = underlying.shape

        # FIXME: not sure this behaviour is intuitive, discuss
        if v := state.get_symbol_from_name_or(self.name, if_not_present=False):
            start, end = state.offset_dict[v]
            # FIXME: This is not a good check for shapes, this condition could
            # be false even if the shapes do not match
            if (nrows * ncols) != (end - start):
                raise ValueError("Cannot parametrize {self.underlying} with variable {v} "
                                 "found in state object, because its shape {(nrow, ncols)} "
                                 "does not match ({self.underlying.shape}). ")

        else:
            state = state.register(Symbol(self.name), shape=(nrows, ncols))

        p = PolyMatrixDict.empty()
        indices = state.get_indices(v)
        for (row, col), index in zip(product(range(nrows), range(ncols)), indices):
            p[row, col] = PolyDict({
                MonomialIndex((VariableIndex(index, 1),)): 1
            })
        
        return state, init_poly_matrix(p, underlying.shape)
