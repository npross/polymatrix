from __future__ import annotations

from numpy.typing import NDArray
import sympy

from polymatrix.expression.mixins.expressionbasemixin import ExpressionBaseMixin
from polymatrix.statemonad import StateMonad


FromSupportedTypes = (
    str
    | NDArray
    | sympy.Matrix | sympy.Expr
    | tuple[...]
    | tuple[tuple[...]]
    | ExpressionBaseMixin
    | StateMonad
)

