from polymatrix.expressionstate import (
    ExpressionState as internal_ExpressionState,
    init_expression_state as internal_init_expression_state,
)

from polymatrix.expression.from_ import (
    from_ as internal_from,
    from_any as internal_from_any,
    from_name as internal_from_name,
    from_names as internal_from_names,
    from_number as internal_from_number,
    from_numbers as internal_from_numbers,
    from_numpy as internal_from_numpy,
    from_state_monad as internal_from_state_monad,
    from_sympy as internal_from_sympy,
)

from polymatrix.expression import (
    Expression as internal_Expression,
    arange as internal_arange,
    ones as internal_ones,
    zeros as internal_zeros,
    v_stack as internal_v_stack,
    h_stack as internal_h_stack,
    product as internal_product,
    concatenate as internal_concatenate,
    block_diag as internal_block_diag,
    lower_triangular as internal_lower_triangular,
    give_name as internal_give_name,
    what_is as internal_what_is,
)

from polymatrix.expression.to import (
    to_constant as internal_to_constant,
    to_sympy as internal_to_sympy
)

from polymatrix.denserepr.from_ import from_polymatrix
from polymatrix.polymatrix.init import to_affine_expression

Expression = internal_Expression
ExpressionState = internal_ExpressionState

init_expression_state = internal_init_expression_state
make_state = init_expression_state

arange = internal_arange
ones = internal_ones
zeros = internal_zeros

v_stack = internal_v_stack
h_stack = internal_h_stack
product = internal_product
concatenate = internal_concatenate
block_diag = internal_block_diag
lower_triangular = internal_lower_triangular
give_name = internal_give_name
what_is = internal_what_is

to_constant_repr = internal_to_constant
to_matrix_repr = from_polymatrix
to_sympy_repr = internal_to_sympy

to_affine = to_affine_expression
to_constant = internal_to_constant
to_dense = from_polymatrix
to_sympy = internal_to_sympy

from_ = internal_from
from_any = internal_from_any
from_name = internal_from_name
from_names = internal_from_names
from_number = internal_from_number
from_numbers = internal_from_numbers
from_numpy = internal_from_numpy
from_state_monad = internal_from_state_monad
from_sympy = internal_from_sympy
