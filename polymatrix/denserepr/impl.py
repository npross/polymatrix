import dataclasses
import itertools
import typing
import numpy as np
import scipy.sparse

from polymatrix.expressionstate import ExpressionState
from polymatrix.expression.utils.getvariableindices import (
    get_variable_indices_from_variable,
)


@dataclasses.dataclass
class DenseReprBufferImpl:
    data: dict[int, np.ndarray] # FIXME: use np.typing.NDArray
    n_row: int
    n_param: int

    def get_max_degree(self):
        return max(degree for degree in self.data.keys())

    def add_buffer(self, index: int):
        if index <= 1:
            buffer = np.zeros((self.n_row, self.n_param**index), dtype=np.double)

        else:
            buffer = scipy.sparse.dok_array(
                (self.n_row, self.n_param**index), dtype=np.double
            )

        self.data[index] = buffer

    def add(self, row: int, col: int, index: int, value: float):
        if index not in self.data:
            self.add_buffer(index)

        self.data[index][row, col] = value

    def __getitem__(self, key):
        if key not in self.data:
            self.add_buffer(key)

        return self.data[key]


# NP: as discussed in meeting #6, this is not actually dense
# NP: also representation here does not mean much, this is actually a series of
# NP: matrices from a cone, consider changing name to reflet this fact.
# NP: also this really needs documentation on how it is indexed, it is _not_
# NP: intuitive from the method names
@dataclasses.dataclass
class DenseReprImpl:
    data: tuple[DenseReprBufferImpl, ...]
    variable_mapping: tuple[int, ...]
    state: ExpressionState

    # NP: get variable value out of solution vector
    def get_value(self, variable, value): # NP: variable index (state), value result from optimiz
        """
        Given 
            variable = 'x'
            value = np.array((0.1, 0.2, 0.3, 0.4, 0.5))
            state = {'x': (1, 3)}
        the function returns np.array((0.2, 0.3)).
        """
        
        variable_indices = get_variable_indices_from_variable(self.state, variable)[1]

        def gen_value_index():
            for variable_index in variable_indices:
                try:
                    yield self.variable_mapping.index(variable_index)
                except ValueError:
                    raise ValueError(
                        f"{variable_index} not found in {self.variable_mapping}"
                    )

        value_index = list(gen_value_index())

        return value[value_index]

    def set_value(self, variable, value):
        variable_indices = get_variable_indices_from_variable(self.state, variable)[1]
        value_index = list(
            self.variable_mapping.index(variable_index)
            for variable_index in variable_indices
        )
        vec = np.zeros(len(self.variable_mapping))
        vec[value_index] = value
        return vec

    # def get_matrix(self, eq_idx: int):
    #     equations = self.data[eq_idx].data

    # NP: Get a function that evaluates the polynomials
    # NP: rename to something more meaningful 
    def get_func(self, eq_idx: int): # -> Callable[[npt.NDArray], [npt.NDArray]
        equations = self.data[eq_idx].data
        max_idx = max(equations.keys())

        # NP: applies slight optimization for small sizes
        if 2 <= max_idx:

            def func(x: np.ndarray) -> np.ndarray:
                if isinstance(x, tuple) or isinstance(x, list):
                    x = np.array(x).reshape(-1, 1)

                elif x.shape[0] == 1:
                    x = x.reshape(-1, 1)

                def acc_x_powers(acc, _):
                    next = (acc @ x.T).reshape(-1, 1)
                    return next

                x_powers = tuple(
                    itertools.accumulate(
                        range(max_idx - 1),
                        acc_x_powers,
                        initial=x,
                    )
                )[1:]

                def gen_value():
                    for idx, equation in equations.items():
                        if idx == 0:
                            yield equation

                        elif idx == 1:
                            yield equation @ x

                        else:
                            yield equation @ x_powers[idx - 2]

                return sum(gen_value())

        else:

            def func(x: np.ndarray) -> np.ndarray:
                if isinstance(x, tuple) or isinstance(x, list):
                    x = np.array(x).reshape(-1, 1)

                def gen_value():
                    for idx, equation in equations.items():
                        if idx == 0:
                            yield equation

                        else:
                            yield equation @ x

                return sum(gen_value())

        return func
