import itertools


# NP: document this function, especially magic return line
def monomial_to_monomial_vector_indices(
    n_var: int,
    variable_indices: tuple[int, ...],
) -> set[int]:
    """
    Given a monomial of degree d, this function returns the indices of a monomial
    vector containing monomials of the same degree.
    
    Given the variable mapping {x : 0, y : 1}, the monomial x*y of the monomial vector

        z = [x**2, x*y, x*y, y**2]

    results in indices = (1, 2).

    Or, the monomial x*y**2 of the monomial vector

        z = [x**3, x**2*y, x**2*y, x*y**2, x**2*y, x*y**2, x*y**2, y**3]

    results in indices = (3, 5, 6)
    """
    
    variable_indices_perm = itertools.permutations(variable_indices)

    return set(
        sum(idx * (n_var**level) for level, idx in enumerate(monomial))
        for monomial in variable_indices_perm
    )
