from __future__ import annotations

import math

from typing import Any, NamedTuple, Iterable 
from math import prod
from dataclassabc import dataclassabc
from dataclasses import replace

from polymatrix.symbol import Symbol 
from polymatrix.utils.deprecation import deprecated
from polymatrix.polymatrix.index import MonomialIndex, VariableIndex

from polymatrix.statemonad import StateCacheMixin

# TODO: move to typing submodule
class IndexRange(NamedTuple):
    start: int
    """ Start of the indices """
    stop: int
    """ End of the indices, this value is not included """

    ncols: int
    """
    Number of columns of the indexed symbol.

    This information is kept here because if a symbol represents a matrix, the
    shape of the matrix is lost.
    """

    def __lt__(self, other):
        return self.start < other.start


@dataclassabc(frozen=True)
class ExpressionState(StateCacheMixin):
    n_variables: int
    """ Number of polynomial variables.

    What is the difference between variables and symbols?
    Suppose M is a 2x2 matrix, then "M" is a symbol, however being a matrix it
    contains 4 variables. The symbol with its shape is a variable.
    """
    
    indices: dict[Symbol, IndexRange]
    """ Map from symbols representing variables to their indices. """

    cache: dict
    """ Cache for StateCacheMixin """

    # --- indexing ---

    def index(self, sym: Symbol, shape: tuple[int, int]) -> tuple[ExpressionState, IndexRange]:
        """ Index a variable and get its index range. """
        if not isinstance(sym, Symbol):
            raise ValueError("State can only index symbols!")

        for s, irange in self.indices.items():
            # Check if already in there
            if s == sym:
                if irange.ncols == shape[1]:
                    return self, irange
                else:
                    nrows = (irange.stop - irange.start) // irange.ncols
                    raise ValueError(f"Symbols must be unique names! Cannot index symbol "
                        f"{sym} with shape {shape} because there is already a symbol "
                        f"with the same name with shape {(nrows, irange.ncols)}")

        # If not save new index
        size = prod(shape)
        index = IndexRange(start=self.n_variables,
                           stop=self.n_variables + size,
                           ncols=shape[1])

        return replace(
            self,
            n_variables=self.n_variables + size,
            indices=self.indices | {sym: index}
        ), index

    def register(self, sym: Symbol, shape: tuple[int, int]) -> ExpressionState:
        """
        Create an index for a variable, but does not return the index. If you
        want the index range use :py:meth:`index`
        """
        state, _ = self.index(sym, shape)
        return state

    # --- retrieval of indices ---

    def get_indices(self, sym: Symbol) -> Iterable[int]:
        """
        Get all indices associated to a symbol.

        When a symbol is not a scalar multiple indices will be associated to
        the symbol, one for each entry.

        See also :py:meth:`get_symbol_indices`, :py:meth:`get_monomial_indices`.
        """
        if sym not in self.indices:
            raise IndexError(f"There is no symbol {sym} in this state object.")

        yield from range(self.indices[sym].start, self.indices[sym].stop)

    def get_indices_as_variable_index(self, sym: Symbol) -> Iterable[VariableIndex]:
        """
        Get all indices associated to a symbol, wrapped in a `VariableIndex`.

        See also :py:meth:`get_indices`,
        :py:class:`polymatrix.polymatrix.index.VariableIndex`.
        """
        yield from (VariableIndex(index=i, power=1)
                    for i in self.get_indices(sym))

    def get_indices_as_monomial_index(self, var: Symbol) -> Iterable[MonomialIndex]:
        """
        Get all indices associated to a symbol, wrapped in a `MonomialIndex`.

        See also :py:meth:`get_indices`,
        :py:class:`polymatrix.polymatrix.index.MonomialIndex`.
        """
        yield from (MonomialIndex((v,))
                    for v in self.get_indices_as_variable_index(var))

    # --- retrieval of shapes ---

    def get_shape(self, sym: Symbol) -> tuple[int, int]:
        if sym not in self.indices:
            raise IndexError(f"There is no symbol {sym} in this state object.")

        idx = self.indices[sym]
        nrows = (idx.stop - idx.start) / idx.ncols
        # FIXME: error message
        assert nrows > 0 and math.isclose(int(nrows), nrows), (
            "State has inconsistent indices, this is an internal "
            "problem. Something went wrong.")
        return (int(nrows), idx.ncols)

    # --- retrieval of symbols ---

    def get_symbol(self, index: int) -> Symbol:
        """ Get the symbol that contains the given index. """
        for symbol, (start, stop, _) in self.indices.items():
            if start <= index < stop:
                return symbol 

        raise IndexError(f"There is no symbol with index {index}.")

    def get_symbol_from_variable_index(self, var: VariableIndex) -> Symbol:
        """ Get the symbol that contains the index inside of a `VariableIndex` """
        return self.get_symbol(var.index)

    def get_symbols_from_monomial_index(self, monomial: MonomialIndex) -> set[Symbol]:
        """ Get all symbols that contain the indices inside of a `MonomialIndex` """
        symbols = set()
        for v in monomial:
            symbols.add(self.get_symbol_from_variable_index(v))

        return symbols

    def get_symbol_from_name_or(self, name: str | Symbol, if_not_present: Any) -> Symbol | Any:
        """
        Get a symbol given its name, or if there is no symbol with the given
        name return what is passed in the `if_not_present` argument.
        """
        for s in self.indices.keys():
            if s == name:
                return s

        return if_not_present

    def get_symbol_from_name(self, name: str) -> Symbol:
        """
        Get a symbol given its name, raises KeyError if there is no symbol with
        the given name.
        """
        if v := self.get_symbol_from_name_or(name, False):
            return v

        raise KeyError(f"There is no symbol named {name}")

    def get_name(self, index: int) -> str:
        """
        Get the name of a variable given its index. 

        What is the difference between the name and the symbol? Suppose M is a
        nonzero matrix, M is the symbol, but the matrix entry in the leftmost
        corner has name M_1. If the symbol represent a scalar, the symbol and
        the name concide.
        """
        for symbol, (start, end, _) in self.indices.items():
            if start <= index < end:
                # Variable is not scalar
                if end - start > 1:
                    return f"{symbol}_{index - start}"

                return symbol

        raise IndexError(f"There is no variable with index {index}.")

    # -- Old API ---

    @property
    @deprecated("replaced by n_variables")
    def n_param(self) -> int:
        return self.n_variables

    @property
    @deprecated("replaced by indices")
    def offset_dict(self):
        return self.indices

    @property
    @deprecated("Support for auxiliary equations was removed")
    def auxillary_equations(self):
        return {}

    @deprecated("replaced by get_variable")
    def get_key_from_offset(self, index: int) -> Symbol:
        return self.get_symbol(index)


def init_expression_state(n_variables: int = 0, indices: dict[Symbol, IndexRange] = {}):
    return ExpressionState(
        n_variables=n_variables,
        indices=indices,
        cache={},
    )
