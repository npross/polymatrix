# Multivariate polynomial library

`polymatrix` is a library to represent and operate on multivariate polynomial matrices. 

It is used to define Sum Of Squares optimization problems.

## Key aspects

Some aspects of the library:

* it has a lazy behavior
    * the library implements operators to build polynomial expressions
    * the actual polynomial is created by calling the [`apply(state)`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/mixins/expressionbasemixin.py) method on the polynomial expression
* a `sympy` expression is converted to a polynomial expression using the [`polymatrix.from_`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/__init__.py) function
* multiple polynomial expressions are combined using functions like 
    * [`polymatrix.v_stack`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/__init__.py)
    * [`polymatrix.block_diag`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/__init__.py).
* polynomial expressions are manipulated using methods like 
    * [`diff`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py), 
    * [`reshape`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py)
    * [`substitute`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py)
    * [`sum`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py)
    * [`to_constant`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py)
* an expression is converted to matrix representation using [`polymatrix.to_matrix_repr`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/__init__.py). To get the actual representation, the [`apply(state)`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/statemonad/mixins/statemonadmixin.py) method needs to be called.

## Example

In this example, two polynomial expressions are defined using sympy expressions

f_1(x_1, x_2) = x_1 + x_2

f_2(x_1, x_2) = x_1 + x_1 x_2

Then, the two expression are combined using the [`__add__`](https://gitlab.nccr-automation.ch/michael.schneeberger/polymatrix/-/blob/main/polymatrix/expression/expression.py) (or equivalently `+` infix) method

f_3(x_1, x_2) = f_1(x_1, x_2) + f_2(x_1, x_2) = 2 x_1 + x_2 + x_1 x_2

Finally, different representations of the polynomial are printed.

``` python
import sympy
import polymatrix

state = polymatrix.init_expression_state()

x1, x2 = sympy.symbols('x1, x2')
x = polymatrix.from_((x1, x2))

f1 = polymatrix.from_(x1 + x2)
f2 = polymatrix.from_(x1 + x1*x2)

f3 = f1 + f2          

# prints the data structure of the expression
# AdditionExprImpl(left=FromSympyExprImpl(data=((x1 + x2,),)), right=FromSympyExprImpl(data=((x1*x2 + x1,),)))
print(f3)

state, poly_matrix = f3.apply(state)

# prints the data structure of the polynomial matrix
# PolyMatrixImpl(terms={(0, 0): {((0, 1), (1, 1)): 1, ((0, 1),): 2, ((1, 1),): 1}}, shape=(1, 1))
print(poly_matrix)

state, sympy_repr = polymatrix.to_sympy(f3,).apply(state)

# prints the sympy representation of the polynomial matrix
# [[x1*x2 + 2*x1 + x2]]
print(sympy_repr)

state, dense_repr = polymatrix.to_dense((f3,), x).apply(state)

# prints the numpy matrix representations of the polynomial matrix
# array([[2., 1.]])
# array([[0. , 0.5, 0.5, 0. ]])
print(dense_repr.data[0][1])               # numpy array
print(dense_repr.data[0][2].toarray())     # sparse scipy array
```
